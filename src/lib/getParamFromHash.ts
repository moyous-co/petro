const getParamFromHash = (url: string, parm: string): string => {
  const re = new RegExp(`#${parm}=(.*)`);
  const match = url.match(re);

  return match ? match[1] : '';
};

export default getParamFromHash;
