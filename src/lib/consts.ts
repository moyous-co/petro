export enum EPanelId {
  INITIAL = 'initial',
  DASHBOARD = 'dashboard',
  ACTIVE_BOARD = 'active-board',
  CREATE_BOARD = 'create-board',
}

export const APP_ID = 7794729;

export const API_URL = 'https://tk-beauty.cs7777.vk.com/al_persik_retro.php';

export const API_VERSION = '5.130';

export const isDevelopment = process.env.NODE_ENV === 'development';
