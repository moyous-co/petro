import React, {
  ChangeEvent,
  FC,
  memo,
  useCallback,
  useEffect,
  useState,
} from 'react';
import {
  CellButton,
  Div,
  FixedLayout,
  FormItem,
  FormLayout,
  FormLayoutGroup,
  FormStatus,
  Input,
  Panel,
  PanelHeader,
  PanelHeaderBack,
  Separator,
  SliderSwitch,
} from '@vkontakte/vkui';
import { useDispatch } from 'react-redux';
import { CirclePicker } from 'react-color';
import { EPanelId } from '../../lib/consts';
import { setActivePanel } from '../../store/actions/general/setActivePanel';
import EBoardPublicity from '../../entities/board/EBoardPublicity';
import EMembersVisibility from '../../entities/board/EMembersVisibility';
import { fireCreateBoard } from '../../store/actions/board/createBoard/fireCreateBoard';

type TProps = {
  id: EPanelId;
};

const CreateBoardPanel: FC<TProps> = ({ id }) => {
  const dispatch = useDispatch();
  const onClick = useCallback(
    () => dispatch(setActivePanel(EPanelId.DASHBOARD)),
    [dispatch],
  );
  const [title, setTitle] = useState('');
  const [color, setColor] = useState('5c9ce6');
  const [boardPublicity, setBoardPublicity] = useState<EBoardPublicity>(
    EBoardPublicity.PRIVATE,
  );
  const [
    membersVisibility,
    setMembersVisibility,
  ] = useState<EMembersVisibility>(EMembersVisibility.ANONYMOUS);
  const [isFormValid, setIsFormValid] = useState(false);
  const [shouldShowFormValidness, setShouldShowFormValidness] = useState(false);

  const onSendForm = useCallback(() => {
    if (isFormValid) {
      dispatch(
        fireCreateBoard({
          title,
          color,
          is_public: boardPublicity === EBoardPublicity.PUBLIC ? '1' : '0',
          is_anonymous:
            membersVisibility === EMembersVisibility.ANONYMOUS ? '1' : '0',
        }),
      );
    } else {
      setShouldShowFormValidness(true);
    }
  }, [isFormValid, dispatch, title, color, boardPublicity, membersVisibility]);

  useEffect(() => {
    if (title === '') {
      setIsFormValid(false);
      return;
    }

    setIsFormValid(true);
    setShouldShowFormValidness(false);
  }, [title]);

  return (
    <Panel id={id}>
      <PanelHeader left={<PanelHeaderBack onClick={onClick} />}>
        Новое ретро
      </PanelHeader>
      {shouldShowFormValidness && (
        <Div>
          <FormStatus header="Некорректное название" mode="error">
            Надо хотя бы как-то назвать новую доску для ретро
          </FormStatus>
        </Div>
      )}
      <FormLayout>
        <FormItem top="Название">
          <Input
            type="text"
            name="petro_board__title"
            value={title}
            onChange={(e: ChangeEvent<HTMLInputElement>) =>
              setTitle(e.target.value)
            }
          />
        </FormItem>
        <FormItem top="Цвет фона плашки доски">
          <CirclePicker
            colors={['#5c9ce6', '#ebadff', '#3dcc4b', '#ffc107', '#ff3347']}
            color={`#${color}`}
            onChangeComplete={(c) => setColor(c.hex.slice(1))}
          />
        </FormItem>
        <FormLayoutGroup mode="vertical">
          <FormItem
            top="Доступность доски"
            bottom="Кому можно будет попасть на ретро"
          >
            <SliderSwitch
              activeValue={boardPublicity}
              name="petro_board__boardPublicity"
              options={[
                {
                  name: 'Приватная',
                  value: EBoardPublicity.PRIVATE,
                },
                {
                  name: 'Открытая',
                  value: EBoardPublicity.PUBLIC,
                },
              ]}
              onSwitch={(value) => setBoardPublicity(value as EBoardPublicity)}
            />
          </FormItem>
          <FormItem
            top="Видимость участников"
            bottom="Будет ли видно авторов карточек и комментариев"
          >
            <SliderSwitch
              activeValue={membersVisibility}
              name="petro_board__membersVisibility"
              options={[
                {
                  name: 'Анонимные',
                  value: EMembersVisibility.ANONYMOUS,
                },
                {
                  name: 'Публичные',
                  value: EMembersVisibility.PUBLIC,
                },
              ]}
              onSwitch={(value) =>
                setMembersVisibility(value as EMembersVisibility)
              }
            />
          </FormItem>
        </FormLayoutGroup>
      </FormLayout>
      <FixedLayout vertical="bottom" filled>
        <Separator wide />
        <CellButton centered onClick={onSendForm}>
          Сохранить
        </CellButton>
      </FixedLayout>
    </Panel>
  );
};

export default memo(CreateBoardPanel);
