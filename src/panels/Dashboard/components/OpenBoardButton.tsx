import React, { FC, memo, useCallback } from 'react';
import { Button } from '@vkontakte/vkui';
import { useDispatch } from 'react-redux';
import { fireOpenBoard } from '../../../store/actions/board/fireOpenBoard';

type TProps = {
  id: number;
};

const OpenBoardButton: FC<TProps> = ({ id }) => {
  const dispatch = useDispatch();
  const onClick = useCallback(() => dispatch(fireOpenBoard(id)), [
    dispatch,
    id,
  ]);

  return (
    <Button size="s" onClick={onClick}>
      К доске
    </Button>
  );
};

export default memo(OpenBoardButton);
