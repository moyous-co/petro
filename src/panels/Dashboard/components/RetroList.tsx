import React, { FC, memo } from 'react';
import { CardGrid, Group, PullToRefresh } from '@vkontakte/vkui';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';
import selectIsBoardsListLoading from '../../../store/selectors/selectIsBoardsListLoading';
import { fireGetBoardsList } from '../../../store/actions/board/getBoardsList/fireGetBoardsList';
import selectBoardsIds from '../../../store/selectors/selectBoardsIds';
import RetroCell from './RetroCell';

const StyledGroup = styled(Group)`
  margin-bottom: 49px;
`;

const RetroList: FC = () => {
  const dispatch = useDispatch();
  const isLoading = useSelector(selectIsBoardsListLoading);
  const ids = useSelector(selectBoardsIds);
  const onRefresh = () => {
    if (!isLoading) {
      dispatch(fireGetBoardsList());
    }
  };

  return (
    <PullToRefresh onRefresh={onRefresh} isFetching={isLoading}>
      <StyledGroup>
        <CardGrid size="s">
          {ids.map((id) => (
            <RetroCell key={id} id={id} />
          ))}
        </CardGrid>
      </StyledGroup>
    </PullToRefresh>
  );
};

export default memo(RetroList);
