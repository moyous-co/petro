import React, { FC, memo, useCallback } from 'react';
import { Icon24Add } from '@vkontakte/icons';
import { CellButton } from '@vkontakte/vkui';
import { useDispatch } from 'react-redux';
import { setActivePanel } from '../../../store/actions/general/setActivePanel';
import { EPanelId } from '../../../lib/consts';

const CreateRetroButton: FC = () => {
  const dispatch = useDispatch();
  const onClick = useCallback(
    () => dispatch(setActivePanel(EPanelId.CREATE_BOARD)),
    [dispatch],
  );

  return (
    <CellButton centered before={<Icon24Add />} onClick={onClick}>
      Новое ретро
    </CellButton>
  );
};

export default memo(CreateRetroButton);
