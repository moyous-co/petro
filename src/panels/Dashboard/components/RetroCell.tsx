import React, { FC, memo, useCallback } from 'react';
import { Card, RichCell, Text } from '@vkontakte/vkui';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';
import {
  Icon16Ghost,
  Icon16Lock,
  Icon16LockOpen,
  Icon16Smile,
} from '@vkontakte/icons';
import selectBoardById from '../../../store/selectors/selectBoardById';
import { TBoard } from '../../../entities/board/TBoard';
import EMembersVisibility from '../../../entities/board/EMembersVisibility';
import EBoardPublicity from '../../../entities/board/EBoardPublicity';
import { fireOpenBoard } from '../../../store/actions/board/fireOpenBoard';

type TProps = {
  id: number;
};

const StyledCard = styled(Card)<{ color: string }>`
  background-color: ${({ color }) => color};
`;

const StyledText = styled(Text)`
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  color: white;
`;

const Title = styled.span`
  display: inline-block;
  flex: 1;
`;

const RetroCell: FC<TProps> = ({ id }) => {
  const { title, isAnonymous, isPublic, color }: TBoard = useSelector(
    selectBoardById(id),
  );
  const dispatch = useDispatch();
  const onClick = useCallback(() => dispatch(fireOpenBoard(id)), [
    dispatch,
    id,
  ]);

  return (
    <StyledCard color={color}>
      <RichCell color="#fff" onClick={onClick}>
        <StyledText weight="regular">
          <Title>{title}</Title>
          <>
            {isAnonymous === EMembersVisibility.ANONYMOUS ? (
              <Icon16Ghost />
            ) : (
              <Icon16Smile />
            )}
            {isPublic === EBoardPublicity.PUBLIC ? (
              <Icon16LockOpen />
            ) : (
              <Icon16Lock />
            )}
          </>
        </StyledText>
      </RichCell>
    </StyledCard>
  );
};

export default memo(RetroCell);
