import React, { FC, memo } from 'react';
import {
  Avatar,
  FixedLayout,
  Panel,
  PanelHeader,
  Separator,
} from '@vkontakte/vkui';
import { useSelector } from 'react-redux';
import { EPanelId } from '../../lib/consts';
import selectUserAvatar from '../../store/selectors/selectUserAvatar';
import CreateRetroButton from './components/CreateRetroButton';
import RetroList from './components/RetroList';

type TProps = {
  id: EPanelId;
};

const DashboardPanel: FC<TProps> = ({ id }) => {
  const avatar = useSelector(selectUserAvatar);

  return (
    <Panel id={id}>
      <PanelHeader left={<Avatar size={36} src={avatar} />}>
        Персиковые Ретроспективы
      </PanelHeader>
      <RetroList />
      <FixedLayout vertical="bottom" filled>
        <Separator wide />
        <CreateRetroButton />
      </FixedLayout>
    </Panel>
  );
};

export default memo(DashboardPanel);
