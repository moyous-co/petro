import React, { FC, memo } from 'react';
import {
  Div,
  FixedLayout,
  InfoRow,
  Panel,
  Placeholder,
  Progress,
} from '@vkontakte/vkui';
import { Icon56ArticleOutline } from '@vkontakte/icons';
import { useSelector } from 'react-redux';
import { EPanelId } from '../../lib/consts';
import selectProgress from '../../store/selectors/selectProgress';
import selectProgressTitle from '../../store/selectors/selectProgressTitle';

type TProps = {
  id: EPanelId;
};

const InitialPanel: FC<TProps> = ({ id }) => {
  const progress = useSelector(selectProgress);
  const progressTitle = useSelector(selectProgressTitle);

  return (
    <Panel id={id}>
      <Placeholder
        icon={<Icon56ArticleOutline />}
        header="Это наши персиковые ретроспективы"
        stretched
      >
        Буквально минутку и мы всё подготовим
      </Placeholder>
      <FixedLayout vertical="bottom">
        <Div>
          <InfoRow header={`${progressTitle}...`}>
            <Progress value={progress} />
          </InfoRow>
        </Div>
      </FixedLayout>
    </Panel>
  );
};

export default memo(InitialPanel);
