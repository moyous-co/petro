import React, { FC, memo, useCallback, useState } from 'react';
import { CellButton } from '@vkontakte/vkui';
import { Icon24Add } from '@vkontakte/icons';
import ExtraRetroCardForm from './ExtraRetroCardForm';

type TProps = {
  id: number;
};

const ExtraRetroCard: FC<TProps> = ({ id }) => {
  const [isFormOpened, setIsFormOpened] = useState(false);
  const onCloseForm = useCallback(() => setIsFormOpened(false), []);
  const onClick = useCallback(() => setIsFormOpened(true), []);

  return (
    <>
      {isFormOpened ? (
        <ExtraRetroCardForm id={id} onCloseForm={onCloseForm} />
      ) : (
        <CellButton before={<Icon24Add />} centered onClick={onClick}>
          Добавить карточку
        </CellButton>
      )}
    </>
  );
};

export default memo(ExtraRetroCard);
