import React, { FC, memo } from 'react';
import { useSelector } from 'react-redux';
import { Avatar } from '@vkontakte/vkui';
import styled from 'styled-components';
import { Icon20UserCircleFillBlue } from '@vkontakte/icons';
import selectIsBoardPublic from '../../../store/selectors/selectIsBoardPublic';
import selectMemberById from '../../../store/selectors/selectMemberById';

type TProps = { userId: number };

const MockAvatar = styled(Avatar)``;

const UserAvatar: FC<TProps> = ({ userId }) => {
  const isBoardPublic = useSelector(selectIsBoardPublic);
  const member = useSelector(selectMemberById(userId));

  if (!isBoardPublic || !member || !member.avatar) {
    return (
      <MockAvatar size={24}>
        <Icon20UserCircleFillBlue />
      </MockAvatar>
    );
  }

  return <Avatar size={24} src={member.avatar} />;
};

export default memo(UserAvatar);
