import React, {
  ChangeEvent,
  FC,
  memo,
  useCallback,
  useEffect,
  useRef,
  useState,
} from 'react';
import { useDispatch } from 'react-redux';
import { Button, Div, FormItem, Textarea } from '@vkontakte/vkui';
import styled from 'styled-components';
import { fireCreateCard } from '../../../store/actions/cards/createCard/fireCreateCard';

type TProps = {
  id: number;
  onCloseForm: () => void;
};

const ActionsBox = styled(Div)`
  display: flex;
  padding-top: 0;
`;

const GroupedButton = styled(Button)`
  &:not(:last-child) {
    margin-right: 8px;
  }
`;

const ExtraRetroCardForm: FC<TProps> = ({ onCloseForm, id }) => {
  const dispatch = useDispatch();
  const [text, setText] = useState('');
  const textareaRef = useRef<HTMLTextAreaElement | null>(null);
  const onSave = useCallback(() => {
    if (text !== '') {
      dispatch(
        fireCreateCard({
          column_id: String(id),
          text,
        }),
      );
      onCloseForm();
    }
  }, [dispatch, id, onCloseForm, text]);

  useEffect(() => {
    textareaRef?.current?.focus();
  }, []);

  return (
    <>
      <FormItem>
        <Textarea
          name="petro_card__text"
          value={text}
          onChange={(e: ChangeEvent<HTMLTextAreaElement>) =>
            setText(e.target.value)
          }
          getRef={textareaRef}
        />
      </FormItem>
      <ActionsBox>
        <GroupedButton stretched mode="secondary" onClick={onCloseForm}>
          Отмена
        </GroupedButton>
        <GroupedButton stretched disabled={text === ''} onClick={onSave}>
          Сохранить
        </GroupedButton>
      </ActionsBox>
    </>
  );
};

export default memo(ExtraRetroCardForm);
