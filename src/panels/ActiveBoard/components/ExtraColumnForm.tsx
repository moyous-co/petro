import React, {
  ChangeEvent,
  FC,
  memo,
  useEffect,
  useRef,
  useState,
} from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Button, Div, FormItem, Input } from '@vkontakte/vkui';
import styled from 'styled-components';
import selectActiveBoardId from '../../../store/selectors/selectActiveBoardId';
import { fireCreateColumn } from '../../../store/actions/columns/createColumn/fireCreateColumn';

type TProps = {
  onCloseForm: () => void;
};

const ActionsBox = styled(Div)`
  display: flex;

  padding-top: 0;
`;

const GroupedButton = styled(Button)`
  &:not(:last-child) {
    margin-right: 8px;
  }
`;

const ExtraColumnForm: FC<TProps> = ({ onCloseForm }) => {
  const boardId = useSelector(selectActiveBoardId);
  const dispatch = useDispatch();
  const [title, setTitle] = useState('');
  const inputRef = useRef<HTMLInputElement | null>(null);
  const onSave = () => {
    if (title !== '') {
      dispatch(
        fireCreateColumn({
          retro_id: String(boardId),
          title,
        }),
      );
      onCloseForm();
    }
  };

  useEffect(() => {
    inputRef?.current?.focus();
  }, []);

  return (
    <>
      <FormItem top="Название колонки">
        <Input
          type="text"
          name="petro_column__title"
          value={title}
          onChange={(e: ChangeEvent<HTMLInputElement>) =>
            setTitle(e.target.value)
          }
          getRef={inputRef}
        />
      </FormItem>
      <ActionsBox>
        <GroupedButton stretched mode="secondary" onClick={onCloseForm}>
          Отмена
        </GroupedButton>
        <GroupedButton stretched disabled={title === ''} onClick={onSave}>
          Сохранить
        </GroupedButton>
      </ActionsBox>
    </>
  );
};

export default memo(ExtraColumnForm);
