import React, {
  ChangeEvent,
  FC,
  memo,
  useCallback,
  useEffect,
  useRef,
  useState,
} from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';
import { Button, Cell, Div, List, Input } from '@vkontakte/vkui';
import selectCardById from '../../../store/selectors/selectCardById';
import { fireCreateComment } from '../../../store/actions/comments/createComment/fireCreateComment';
import UserAvatar from './UserAvatar';

type TProps = {
  cardId: number;
};

const StyledInput = styled(Input)`
  margin-top: 8px;
`;

const ActionsBox = styled(Div)`
  display: flex;
  align-items: flex-end;
  padding: 8px 0 0;
`;

const GroupedButton = styled(Button)`
  &:not(:last-child) {
    margin-right: 8px;
  }
`;

const Comments: FC<TProps> = ({ cardId }) => {
  const dispatch = useDispatch();
  const inputRef = useRef<HTMLInputElement | null>(null);
  const [text, setText] = useState('');
  const { comments } = useSelector(selectCardById(cardId));
  const onSave = useCallback(() => {
    dispatch(
      fireCreateComment({
        card_id: String(cardId),
        text,
      }),
    );
    setText('');
  }, [cardId, dispatch, text]);

  useEffect(() => {
    inputRef?.current?.focus();
  }, []);

  return (
    <>
      <List>
        {comments.map(({ text, userId, id }) => (
          <Cell disabled before={<UserAvatar userId={userId} />} key={id}>
            {text}
          </Cell>
        ))}
      </List>
      <StyledInput
        type="text"
        name="petro_comment__text"
        value={text}
        onChange={(e: ChangeEvent<HTMLInputElement>) => setText(e.target.value)}
        getRef={inputRef}
      />
      <ActionsBox>
        <GroupedButton disabled={text === ''} onClick={onSave}>
          Сохранить
        </GroupedButton>
      </ActionsBox>
    </>
  );
};

export default memo(Comments);
