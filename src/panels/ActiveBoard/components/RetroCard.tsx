import React, { FC, memo, useCallback, useState } from 'react';
import { Cell, Div, Separator } from '@vkontakte/vkui';
import styled from 'styled-components';
import { useSelector } from 'react-redux';
import ReadRetroCard from './ReadRetroCard';
import EditRetroCardForm from './EditRetroCardForm';
import ECardMode from './ECardMode';
import Reactions from './Reactions';
import Comments from './Comments';
import selectCardById from '../../../store/selectors/selectCardById';

type TProps = {
  id: number;
};

const StyledBox = styled(Div)`
  display: flex;
  align-items: center;
  padding: 4px 0;
`;

const StyledCell = styled(Cell)`
  & .vkuiSimpleCell--android,
  .vkuiSimpleCell--vkcom {
    padding: 0 4px;
  }

  & .vkuiSimpleCell__children {
    flex: 1;
  }
`;

const RetroCard: FC<TProps> = ({ id }) => {
  const { comments } = useSelector(selectCardById(id));
  const [cardMode, setCardMode] = useState<ECardMode>(ECardMode.READ);
  const [isCommentsOpened, setIsCommentsOpened] = useState(false);
  const onClick = useCallback((mode: ECardMode) => setCardMode(mode), []);

  return (
    <StyledCell disabled>
      {cardMode === ECardMode.READ && <ReadRetroCard cardId={id} />}
      {cardMode === ECardMode.EDIT && (
        <EditRetroCardForm cardId={id} onCloseForm={onClick} />
      )}
      <Separator wide />
      <StyledBox>
        <Reactions
          cardId={id}
          onClickChangeMode={onClick}
          mode={cardMode}
          onClickComments={() => setIsCommentsOpened(!isCommentsOpened)}
          isCommentsOpened={isCommentsOpened}
          commentsCount={comments.length}
        />
      </StyledBox>
      <Separator wide />
      {isCommentsOpened && <Comments cardId={id} />}
    </StyledCell>
  );
};

export default memo(RetroCard);
