import React, { FC, memo, useMemo } from 'react';
import {
  Icon24Like,
  Icon24LikeOutline,
  Icon24DeleteOutline,
  Icon24CommentOutline,
  Icon24Comment,
} from '@vkontakte/icons';
import { useDispatch, useSelector } from 'react-redux';
import { IconButton, Text } from '@vkontakte/vkui';
import styled from 'styled-components';
import selectReactionsByCardId from '../../../store/selectors/selectReactionsByCardId';
import selectUserId from '../../../store/selectors/selectUserId';
import { fireCreateReaction } from '../../../store/actions/reactions/createReaction/fireCreateReaction';
import { fireDeleteReaction } from '../../../store/actions/reactions/deleteReaction/fireDeleteReaction';
import EditRetroCardIcon from './EditRetroCardIcon';
import ECardMode from './ECardMode';
import { fireDeleteCard } from '../../../store/actions/cards/deleteCard/fireDeleteCard';
import selectCardById from '../../../store/selectors/selectCardById';

type TProps = {
  cardId: number;
  onClickChangeMode: (mode: ECardMode) => void;
  mode: ECardMode;
  onClickComments: () => void;
  isCommentsOpened: boolean;
  commentsCount: number;
};

const StyledIconButton = styled(IconButton)`
  &:not(:last-child) {
    margin-right: 4px;
  }

  &.vkuiIconButton {
    height: auto;
  }

  &.vkuiIconButton .vkuiIcon--24 {
    padding: 8px;
  }
`;

const PrettyIconButton = styled(StyledIconButton)`
  color: var(--dynamic_red);
`;

const ReactionsBox = styled.div`
  display: flex;
  align-items: center;
  flex: 1;
`;

const StyledText = styled(Text)`
  margin-right: 12px;

  color: var(--dynamic_red);
`;

const PrettyCommentsButton = styled(StyledIconButton)`
  color: var(--dynamic_blue);
`;

const CommentsText = styled(Text)`
  color: var(--dynamic_blue);
`;

const Reactions: FC<TProps> = ({
  cardId,
  mode,
  onClickChangeMode,
  onClickComments,
  isCommentsOpened,
  commentsCount,
}) => {
  const dispatch = useDispatch();
  const { columnId } = useSelector(selectCardById(cardId));
  const reactions = useSelector(selectReactionsByCardId(cardId));
  const userId = useSelector(selectUserId);
  const ownReaction = useMemo(() => {
    if (!userId) {
      return false;
    }

    return reactions.find(({ creatorId }) => creatorId === userId);
  }, [reactions, userId]);
  const onAddLike = () => {
    dispatch(
      fireCreateReaction({
        card_id: String(cardId),
      }),
    );
  };

  const onRemoveLike = () => {
    if (ownReaction) {
      dispatch(
        fireDeleteReaction({
          id: ownReaction.id,
          cardId,
        }),
      );
    }
  };

  const onDeleteCard = () => {
    dispatch(fireDeleteCard({ id: cardId, column_id: columnId }));
  };

  if (!userId) {
    return null;
  }

  return (
    <>
      <ReactionsBox>
        <PrettyIconButton onClick={ownReaction ? onRemoveLike : onAddLike}>
          {ownReaction ? <Icon24Like /> : <Icon24LikeOutline />}
        </PrettyIconButton>{' '}
        <StyledText weight="medium">
          {reactions.length > 0 ? reactions.length : ''}
        </StyledText>
        <PrettyCommentsButton onClick={onClickComments}>
          {isCommentsOpened ? <Icon24Comment /> : <Icon24CommentOutline />}
        </PrettyCommentsButton>
        <CommentsText weight="medium">
          {commentsCount > 0 ? commentsCount : ''}
        </CommentsText>
      </ReactionsBox>
      {mode === ECardMode.EDIT && (
        <StyledIconButton onClick={onDeleteCard}>
          <Icon24DeleteOutline />
        </StyledIconButton>
      )}
      <EditRetroCardIcon
        mode={mode}
        onClick={(mode) => onClickChangeMode(mode)}
      />
    </>
  );
};

export default memo(Reactions);
