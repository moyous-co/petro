enum ECardMode {
  READ = 'read',
  EDIT = 'edit',
}

export default ECardMode;
