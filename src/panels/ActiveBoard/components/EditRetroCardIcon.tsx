import React, { FC, memo } from 'react';
import { Icon24Cancel, Icon24PenOutline } from '@vkontakte/icons';
import { IconButton } from '@vkontakte/vkui';
import styled from 'styled-components';
import ECardMode from './ECardMode';

type TProps = {
  onClick: (mode: ECardMode) => void;
  mode: ECardMode;
};

const StyledIconButton = styled(IconButton)`
  &:not(:last-child) {
    margin-right: 4px;
  }

  &.vkuiIconButton {
    height: auto;
  }

  &.vkuiIconButton .vkuiIcon--24 {
    padding: 8px;
  }
`;

const EditRetroCardIcon: FC<TProps> = ({ onClick, mode }) => (
  <StyledIconButton
    onClick={() =>
      onClick(mode === ECardMode.EDIT ? ECardMode.READ : ECardMode.EDIT)
    }
  >
    {mode === ECardMode.READ && <Icon24PenOutline />}
    {mode === ECardMode.EDIT && <Icon24Cancel />}
  </StyledIconButton>
);

export default memo(EditRetroCardIcon);
