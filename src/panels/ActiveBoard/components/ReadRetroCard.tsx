import React, { FC, memo } from 'react';
import { Text } from '@vkontakte/vkui';
import styled from 'styled-components';
import { useSelector } from 'react-redux';
import selectCardById from '../../../store/selectors/selectCardById';

type TProps = {
  cardId: number;
};

const StyledText = styled(Text)`
  margin: 0 4px 8px;
  white-space: pre-wrap;
`;

const ReadRetroCard: FC<TProps> = ({ cardId }) => {
  const { text } = useSelector(selectCardById(cardId));

  return <StyledText weight="regular">{text}</StyledText>;
};

export default memo(ReadRetroCard);
