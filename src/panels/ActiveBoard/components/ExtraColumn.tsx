import React, { FC, memo, useCallback, useState } from 'react';
import { Card, CellButton } from '@vkontakte/vkui';
import { Icon24Add } from '@vkontakte/icons';
import styled from 'styled-components';
import ExtraColumnForm from './ExtraColumnForm';

const StyledCard = styled(Card)`
  &.vkuiCard {
    width: 30%;
  }
`;

const ExtraColumn: FC = () => {
  const [isFormOpened, setIsFormOpened] = useState(false);
  const onCloseForm = useCallback(() => setIsFormOpened(false), []);
  const onClick = useCallback(() => setIsFormOpened(true), []);

  return (
    <StyledCard>
      {isFormOpened ? (
        <ExtraColumnForm onCloseForm={onCloseForm} />
      ) : (
        <CellButton before={<Icon24Add />} centered onClick={onClick}>
          Добавить колонку
        </CellButton>
      )}
    </StyledCard>
  );
};

export default memo(ExtraColumn);
