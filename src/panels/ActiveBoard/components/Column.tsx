import React, {
  ChangeEvent,
  FC,
  Fragment,
  memo,
  useCallback,
  useEffect,
  useRef,
  useState,
} from 'react';
import {
  Button,
  Card,
  Div,
  Headline,
  IconButton,
  Input,
  List,
  Separator,
} from '@vkontakte/vkui';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';
import { Icon24DeleteOutline } from '@vkontakte/icons';
import selectColumnById from '../../../store/selectors/selectColumnById';
import RetroCard from './RetroCard';
import ExtraRetroCard from './ExtraRetroCard';
import { fireEditColumn } from '../../../store/actions/columns/editColumn/fireEditColumn';
import { fireDeleteColumn } from '../../../store/actions/columns/deleteColumn/fireDeleteColumn';

type TProps = {
  id: number;
};

const StyledHeadline = styled(Headline)`
  cursor: pointer;
  flex: 1;
  margin: 10px 16px 10px 12px;
`;

const StyledTitleBox = styled(Div)`
  display: flex;
  align-items: center;

  padding: 4px;

  & .vkuiIconButton .vkuiIcon--24 {
    padding: 10px;
  }
`;

const StyledInput = styled(Input)`
  flex: 1;
`;

const StyledIconButton = styled(IconButton)`
  height: auto;

  &.vkuiIconButton .vkuiIcon--24 {
    padding: 8px;
  }
`;

const StyledCard = styled(Card)`
  overflow: hidden;

  &.vkuiCard {
    width: 30%;
  }
`;

const StyledButton = styled(Button)`
  margin: 2px 0;
`;

enum ETitleMode {
  READ = 'read',
  CONFIRM_DELETE = 'cofirm-delete',
  EDIT = 'edit',
}

const Column: FC<TProps> = ({ id }) => {
  const dispatch = useDispatch();
  const { title, cardsIds } = useSelector(selectColumnById(id));
  const inputRef = useRef<HTMLInputElement | null>(null);
  const [editingTitle, setEditingTitle] = useState(title);
  const [titleMode, switchTitleMode] = useState<ETitleMode>(ETitleMode.READ);
  const onClickTitle = useCallback(() => switchTitleMode(ETitleMode.EDIT), []);
  const onClickDelete = useCallback(
    () => switchTitleMode(ETitleMode.CONFIRM_DELETE),
    [],
  );
  const onBlurEdit = useCallback(() => {
    switchTitleMode(ETitleMode.READ);
    if (editingTitle !== title) {
      dispatch(
        fireEditColumn({
          id: String(id),
          title: editingTitle,
        }),
      );
    }
    setEditingTitle(title);
  }, [dispatch, editingTitle, id, title]);

  const onLeaveDelete = useCallback(() => {
    switchTitleMode(ETitleMode.READ);
  }, []);

  const onDelete = useCallback(() => {
    dispatch(fireDeleteColumn({ id }));
  }, [dispatch, id]);

  useEffect(() => {
    if (titleMode === ETitleMode.EDIT) {
      inputRef?.current?.focus();
    }
  }, [titleMode]);

  useEffect(() => {
    setEditingTitle(title);
  }, [title]);

  const [isVisibleDelete, setIsVisibleDelete] = useState(false);
  const onEnterBox = useCallback(() => setIsVisibleDelete(true), []);
  const onLeaveBox = useCallback(() => setIsVisibleDelete(false), []);

  return (
    <StyledCard>
      <StyledTitleBox onMouseEnter={onEnterBox} onMouseLeave={onLeaveBox}>
        {titleMode === ETitleMode.EDIT && (
          <StyledInput
            type="text"
            name="petro_column__title"
            value={editingTitle}
            onChange={(e: ChangeEvent<HTMLInputElement>) =>
              setEditingTitle(e.target.value)
            }
            getRef={inputRef}
            onBlur={onBlurEdit}
          />
        )}
        {titleMode === ETitleMode.READ && (
          <>
            <StyledHeadline weight="regular" onClick={onClickTitle}>
              {title}
            </StyledHeadline>
            {isVisibleDelete && (
              <StyledIconButton onClick={onClickDelete}>
                <Icon24DeleteOutline />
              </StyledIconButton>
            )}
          </>
        )}
        {titleMode === ETitleMode.CONFIRM_DELETE && (
          <StyledButton
            mode="destructive"
            stretched
            size="m"
            onMouseLeave={onLeaveDelete}
            onClick={onDelete}
          >
            Удалить колонку
          </StyledButton>
        )}
      </StyledTitleBox>
      <Separator wide />
      <ExtraRetroCard id={id} />
      {cardsIds.length > 0 && (
        <List>
          {cardsIds.map((id) => (
            <Fragment key={id}>
              <Separator wide />
              <RetroCard key={id} id={id} />
            </Fragment>
          ))}
        </List>
      )}
    </StyledCard>
  );
};

export default memo(Column);
