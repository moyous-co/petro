import React, {
  ChangeEvent,
  FC,
  memo,
  useCallback,
  useRef,
  useState,
} from 'react';
import { Button, Div, Textarea } from '@vkontakte/vkui';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';
import { fireEditCard } from '../../../store/actions/cards/editCard/fireEditCard';
import selectCardById from '../../../store/selectors/selectCardById';
import ECardMode from './ECardMode';

type TProps = {
  cardId: number;
  onCloseForm: (mode: ECardMode) => void;
};

const ActionsBox = styled(Div)`
  display: flex;
  padding-left: 0;
  padding-right: 0;
`;

const GroupedButton = styled(Button)`
  &:not(:last-child) {
    margin-right: 8px;
  }
`;

const EditRetroCardForm: FC<TProps> = ({ cardId, onCloseForm }) => {
  const dispatch = useDispatch();
  const { text: initialText } = useSelector(selectCardById(cardId));
  const [text, setText] = useState(initialText);
  const textareaRef = useRef<HTMLTextAreaElement | null>(null);
  const onSave = useCallback(() => {
    if (text !== initialText) {
      dispatch(
        fireEditCard({
          id: String(cardId),
          text,
        }),
      );
    }
    onCloseForm(ECardMode.READ);
  }, [text, initialText, dispatch, cardId, onCloseForm]);

  return (
    <>
      <Textarea
        name="petro_card__text"
        value={text}
        onChange={(e: ChangeEvent<HTMLTextAreaElement>) =>
          setText(e.target.value)
        }
        getRef={textareaRef}
      />
      <ActionsBox>
        <GroupedButton
          stretched
          mode="secondary"
          onClick={() => onCloseForm(ECardMode.READ)}
        >
          Отмена
        </GroupedButton>
        <GroupedButton stretched disabled={text === ''} onClick={onSave}>
          Сохранить
        </GroupedButton>
      </ActionsBox>
    </>
  );
};

export default memo(EditRetroCardForm);
