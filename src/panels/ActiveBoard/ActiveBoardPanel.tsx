import React, { FC, memo, useCallback } from 'react';
import {
  CardScroll,
  CellButton,
  Div,
  FixedLayout,
  Panel,
  PanelHeader,
  PanelHeaderBack,
  Separator,
} from '@vkontakte/vkui';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';
import bridge from '@vkontakte/vk-bridge';
import { Icon24ShareOutline } from '@vkontakte/icons';
import { APP_ID, EPanelId } from '../../lib/consts';
import { setActivePanel } from '../../store/actions/general/setActivePanel';
import selectActiveBoardId from '../../store/selectors/selectActiveBoardId';
import selectBoardById from '../../store/selectors/selectBoardById';
import ExtraColumn from './components/ExtraColumn';
import selectActiveBoardColumnsIds from '../../store/selectors/selectActiveBoardColumnsIds';
import Column from './components/Column';

type TProps = {
  id: EPanelId;
};

const StyledCardScroll = styled(CardScroll)`
  & .vkuiCardScroll__in {
    align-items: flex-start;
  }

  & .vkuiCardScroll__gap {
    width: 0;
  }
`;

const ActiveBoardPanel: FC<TProps> = ({ id }) => {
  const dispatch = useDispatch();
  const onClick = useCallback(
    () => dispatch(setActivePanel(EPanelId.DASHBOARD)),
    [dispatch],
  );
  const boardId = useSelector(selectActiveBoardId);
  const { title } = useSelector(selectBoardById(boardId));
  const columnsIds = useSelector(selectActiveBoardColumnsIds);
  const onShare = () => {
    bridge.send('VKWebAppShare', {
      link: `https://vk.com/app${APP_ID}#retro=${boardId}`,
    });
  };

  return (
    <Panel id={id}>
      <PanelHeader left={<PanelHeaderBack onClick={onClick} />}>
        {title}
      </PanelHeader>
      <Div>
        <StyledCardScroll size="s">
          {columnsIds.map((id) => (
            <Column key={id} id={id} />
          ))}
          <ExtraColumn />
        </StyledCardScroll>
      </Div>
      <FixedLayout vertical="bottom" filled>
        <Separator wide />
        <CellButton centered onClick={onShare} before={<Icon24ShareOutline />}>
          Поделиться
        </CellButton>
      </FixedLayout>
    </Panel>
  );
};

export default memo(ActiveBoardPanel);
