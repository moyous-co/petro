export type TRawColumn = {
  id: string;
  retro_id: string;
  title: string;
  created_at: string;
  updated_at: string;
};
