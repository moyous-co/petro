import { TRawColumn } from './TRawColumn';
import { TColumn } from './TColumn';

const cleanColumn = ({
  id: rawId,
  title,
  retro_id: rawRetroId,
}: TRawColumn): TColumn => ({
  id: Number(rawId),
  retroId: Number(rawRetroId),
  title,
  cardsIds: [],
});

export default cleanColumn;
