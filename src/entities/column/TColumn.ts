export type TColumn = {
  id: number;
  title: string;
  retroId: number;
  cardsIds: number[];
};
