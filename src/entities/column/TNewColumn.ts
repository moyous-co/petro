export type TNewColumn = {
  retro_id: string;
  title: string;
};
