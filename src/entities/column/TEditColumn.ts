export type TEditColumn = {
  id: string;
  title: string;
};
