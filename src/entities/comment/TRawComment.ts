export type TRawComment = {
  id: string;
  retro_id: string;
  card_id: string;
  user_id: string;
  text: string;
  created_at: string;
  updated_at: string;
};
