export type TComment = {
  id: number;
  retroId: number;
  cardId: number;
  userId: number;
  text: string;
};
