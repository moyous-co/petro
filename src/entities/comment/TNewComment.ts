export type TNewComment = {
  text: string;
  card_id: string;
};
