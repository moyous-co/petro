import { TRawComment } from './TRawComment';
import { TComment } from './TComment';

const cleanComment = ({
  id: rawId,
  card_id: rawCardId,
  retro_id: rawRetroId,
  user_id: rawUserId,
  text,
}: TRawComment): TComment => ({
  id: Number(rawId),
  cardId: Number(rawCardId),
  retroId: Number(rawRetroId),
  userId: Number(rawUserId),
  text,
});

export default cleanComment;
