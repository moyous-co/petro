export type TMember = {
  id: number;
  avatar: string | null;
};
