import { TMember } from './TMember';
import { TRawUser } from '../user/TRawUser';

const cleanMember = ({ id, photo_50: avatar }: TRawUser): TMember => ({
  avatar,
  id,
});

export default cleanMember;
