export type TRawMember = {
  retro_id: string;
  user_id: string;
  created_at: string;
};
