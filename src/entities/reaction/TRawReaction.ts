export type TRawReaction = {
  id: string;
  type: string;
  card_id: string;
  user_id: string;
};
