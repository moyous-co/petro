import EReactionType from './EReactionType';

export type TReaction = {
  id: number;
  type: EReactionType;
  cardId: number;
  creatorId: number;
};
