import { TRawReaction } from './TRawReaction';
import { TReaction } from './TReaction';
import EReactionType from './EReactionType';

const cleanReaction = ({
  id: rawId,
  card_id: rawCardId,
  user_id: rawCreatorId,
}: TRawReaction): TReaction => ({
  id: Number(rawId),
  type: EReactionType.LIKE,
  cardId: Number(rawCardId),
  creatorId: Number(rawCreatorId),
});

export default cleanReaction;
