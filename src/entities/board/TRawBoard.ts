export type TRawBoard = {
  is_anonymous: '0' | '1';
  is_public: '0' | '1';
  color: string;
  id: string;
  title: string;
  created_at: string;
  updated_at: string;
  user_id?: string;
};
