enum EMembersVisibility {
  PUBLIC = 'public',
  ANONYMOUS = 'anonymous',
}

export default EMembersVisibility;
