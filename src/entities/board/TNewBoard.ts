export type TNewBoard = {
  title: string;
  color: string;
  is_public: '0' | '1';
  is_anonymous: '0' | '1';
};
