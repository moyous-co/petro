import EBoardPublicity from './EBoardPublicity';
import EMembersVisibility from './EMembersVisibility';

export type TBoard = {
  title: string;
  color: string;
  creatorId: number | null;
  updatedAt: number;
  createdAt: number;
  id: number;
  isPublic: EBoardPublicity;
  isAnonymous: EMembersVisibility;
};
