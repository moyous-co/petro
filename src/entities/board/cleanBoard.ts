import { TRawBoard } from './TRawBoard';
import { TBoard } from './TBoard';
import EBoardPublicity from './EBoardPublicity';
import EMembersVisibility from './EMembersVisibility';

const cleanBoard = ({
  created_at: rawCreatedAt,
  is_anonymous: rawIsAnonymous,
  is_public: rawIsPublic,
  id: rawId,
  color: rawColor,
  title,
  updated_at: rawUpdatedAt,
  user_id: rawUserId,
}: TRawBoard): TBoard => ({
  id: Number(rawId),
  title,
  color: `#${rawColor}`,
  creatorId: rawUserId ? Number(rawUserId) : null,
  updatedAt: Number(rawUpdatedAt) * 1000,
  createdAt: Number(rawCreatedAt) * 1000,
  isPublic:
    rawIsPublic === '1' ? EBoardPublicity.PUBLIC : EBoardPublicity.PRIVATE,
  isAnonymous:
    rawIsAnonymous === '1'
      ? EMembersVisibility.ANONYMOUS
      : EMembersVisibility.PUBLIC,
});

export default cleanBoard;
