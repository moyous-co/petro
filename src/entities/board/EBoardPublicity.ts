enum EBoardPublicity {
  PUBLIC = 'public',
  PROTECTED = 'protected',
  PRIVATE = 'private',
}

export default EBoardPublicity;
