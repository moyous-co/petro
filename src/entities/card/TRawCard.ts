import { TRawReaction } from '../reaction/TRawReaction';
import { TRawComment } from '../comment/TRawComment';

export type TRawCard = {
  id: string;
  user_id: string;
  column_id: string;
  text: string;
  created_at: string;
  updated_at: string;
  reactions: TRawReaction[];
  comments: TRawComment[];
};
