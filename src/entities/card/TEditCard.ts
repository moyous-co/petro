export type TEditCard = {
  id: string;
  text: string;
};
