import { TReaction } from '../reaction/TReaction';
import { TComment } from '../comment/TComment';

export type TCard = {
  id: number;
  creatorId: number;
  columnId: number;
  text: string;
  reactions: TReaction[];
  comments: TComment[];
};
