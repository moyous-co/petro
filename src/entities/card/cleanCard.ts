import { TRawCard } from './TRawCard';
import { TCard } from './TCard';
import cleanReaction from '../reaction/cleanReaction';
import cleanComment from '../comment/cleanComment';

const cleanCard = ({
  id: rawId,
  user_id: rawCreatorId,
  column_id: rawColumnId,
  text,
  reactions: rawReactions = [],
  comments: rawComments = [],
}: TRawCard): TCard => ({
  id: Number(rawId),
  creatorId: Number(rawCreatorId),
  columnId: Number(rawColumnId),
  text,
  reactions: rawReactions.map((rawReaction) => cleanReaction(rawReaction)),
  comments: rawComments.map((rawComment) => cleanComment(rawComment)),
});

export default cleanCard;
