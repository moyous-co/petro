export type TRawUser = {
  id: number;
  first_name: string;
  last_name: string;
  photo_100: string;
  photo_50: string;
};
