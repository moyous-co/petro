import { TRawUser } from './TRawUser';
import { TUser } from './TUser';

const cleanUser = ({
  id,
  first_name: name,
  last_name: surname,
  photo_100: avatar,
}: TRawUser): TUser => ({
  avatar,
  id,
  name,
  surname,
});

export default cleanUser;
