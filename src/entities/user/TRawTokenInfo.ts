export type TRawTokenInfo = {
  access_token: string;
  scope: string;
};
