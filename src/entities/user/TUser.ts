export type TUser = {
  avatar: string;
  id: number;
  name: string;
  surname: string;
};
