import { SagaIterator } from 'redux-saga';
import { takeLatest } from 'redux-saga/effects';
import { FIRE_INITIAL_SETUP } from './actions/general/fireInitialSetup';
import initialSetupSaga from './sagas/initialSetupSaga';
import { FIRE_CREATE_BOARD } from './actions/board/createBoard/fireCreateBoard';
import createBoardSaga from './sagas/createBoardSaga';
import { FIRE_GET_BOARDS_LIST } from './actions/board/getBoardsList/fireGetBoardsList';
import getBoardsListSaga from './sagas/getBoardsListSaga';
import { FIRE_DELETE_BOARD } from './actions/board/deleteBoard/fireDeleteBoard';
import deleteBoardSaga from './sagas/deleteBoardSaga';
import { FIRE_OPEN_BOARD } from './actions/board/fireOpenBoard';
import openBoardSaga from './sagas/openBoardSaga';
import { FIRE_GET_COLUMNS } from './actions/columns/getColumns/fireGetColumns';
import getColumnsSaga from './sagas/getColumnsSaga';
import { FIRE_GET_CARDS } from './actions/cards/getCards/fireGetCards';
import getCardsSaga from './sagas/getCardsSaga';
import { FIRE_CREATE_COLUMN } from './actions/columns/createColumn/fireCreateColumn';
import createColumnSaga from './sagas/createColumnSaga';
import { FIRE_CREATE_CARD } from './actions/cards/createCard/fireCreateCard';
import createCardSaga from './sagas/createCardSaga';
import { FIRE_EDIT_COLUMN } from './actions/columns/editColumn/fireEditColumn';
import editColumnSaga from './sagas/editColumnSaga';
import { FIRE_DELETE_COLUMN } from './actions/columns/deleteColumn/fireDeleteColumn';
import deleteColumnSaga from './sagas/deleteColumnSaga';
import { FIRE_EDIT_CARD } from './actions/cards/editCard/fireEditCard';
import editCardSaga from './sagas/editCardSaga';
import { FIRE_CREATE_REACTION } from './actions/reactions/createReaction/fireCreateReaction';
import createReactionSaga from './sagas/createReactionSaga';
import { FIRE_DELETE_REACTION } from './actions/reactions/deleteReaction/fireDeleteReaction';
import deleteReactionSaga from './sagas/deleteReactionSaga';
import { FIRE_DELETE_CARD } from './actions/cards/deleteCard/fireDeleteCard';
import deleteCardSaga from './sagas/deleteCardSaga';
import { FIRE_DELETE_COMMENT } from './actions/comments/deleteComment/fireDeleteComment';
import deleteCommentSaga from './sagas/deleteCommentSaga';
import { FIRE_CREATE_COMMENT } from './actions/comments/createComment/fireCreateComment';
import createCommentSaga from './sagas/createCommentSaga';

export default function* rootSaga(): SagaIterator<void> {
  yield takeLatest(FIRE_INITIAL_SETUP, initialSetupSaga);
  yield takeLatest(FIRE_CREATE_BOARD, createBoardSaga);
  yield takeLatest(FIRE_GET_BOARDS_LIST, getBoardsListSaga);
  yield takeLatest(FIRE_DELETE_BOARD, deleteBoardSaga);
  yield takeLatest(FIRE_OPEN_BOARD, openBoardSaga);
  yield takeLatest(FIRE_GET_COLUMNS, getColumnsSaga);
  yield takeLatest(FIRE_GET_CARDS, getCardsSaga);
  yield takeLatest(FIRE_CREATE_COLUMN, createColumnSaga);
  yield takeLatest(FIRE_CREATE_CARD, createCardSaga);
  yield takeLatest(FIRE_EDIT_COLUMN, editColumnSaga);
  yield takeLatest(FIRE_DELETE_COLUMN, deleteColumnSaga);
  yield takeLatest(FIRE_EDIT_CARD, editCardSaga);
  yield takeLatest(FIRE_CREATE_REACTION, createReactionSaga);
  yield takeLatest(FIRE_DELETE_REACTION, deleteReactionSaga);
  yield takeLatest(FIRE_DELETE_CARD, deleteCardSaga);
  yield takeLatest(FIRE_DELETE_COMMENT, deleteCommentSaga);
  yield takeLatest(FIRE_CREATE_COMMENT, createCommentSaga);
}
