import { combineReducers } from 'redux';
import { TAppState } from './TAppState';
import userReducer from './reducers/user/userReducer';
import generalReducer from './reducers/general/generalReducer';
import boardsListReducer from './reducers/boardsList/boardsListReducer';
import activeBoardReducer from './reducers/activeBoard/activeBoardReducer';
import cardsReducer from './reducers/cards/cardsReducer';
import columnsReducer from './reducers/columns/columnsReducer';
import reactionsReducer from './reducers/reactions/reactionsReducer';
import commentsReducer from './reducers/comments/commentsReducer';
import membersReducer from './reducers/members/membersReducer';

const rootReducer = combineReducers<TAppState>({
  user: userReducer,
  general: generalReducer,
  boardsList: boardsListReducer,
  activeBoard: activeBoardReducer,
  cards: cardsReducer,
  columns: columnsReducer,
  reactions: reactionsReducer,
  comments: commentsReducer,
  members: membersReducer,
});

export default rootReducer;
