import { TAppState } from '../TAppState';

const selectActiveBoardColumnsIds = (state: TAppState): number[] =>
  state.activeBoard.columnIds;

export default selectActiveBoardColumnsIds;
