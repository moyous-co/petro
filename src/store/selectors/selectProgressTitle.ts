import { TAppState } from '../TAppState';

const selectProgressTitle = (state: TAppState): string =>
  state.general.progressTitle;

export default selectProgressTitle;
