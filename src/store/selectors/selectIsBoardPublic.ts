import { TAppState } from '../TAppState';
import EMembersVisibility from '../../entities/board/EMembersVisibility';

const selectIsBoardPublic = (state: TAppState): boolean =>
  !!state.activeBoard.retroId &&
  state.boardsList.itemsMap[state.activeBoard.retroId].isAnonymous ===
    EMembersVisibility.PUBLIC;

export default selectIsBoardPublic;
