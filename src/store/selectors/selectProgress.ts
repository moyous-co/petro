import { TAppState } from '../TAppState';

const selectProgress = (state: TAppState): number => state.general.progress;

export default selectProgress;
