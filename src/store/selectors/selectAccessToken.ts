import { TAppState } from '../TAppState';

const selectAccessToken = (state: TAppState): string =>
  state.user.accessToken || '';

export default selectAccessToken;
