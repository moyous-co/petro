import { TAppState } from '../TAppState';

const selectIsBoardsListLoaded = (state: TAppState): boolean =>
  state.boardsList.isLoaded;

export default selectIsBoardsListLoaded;
