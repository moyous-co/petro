import { TAppState } from '../TAppState';

const selectIsBoardsListLoading = (state: TAppState): boolean =>
  state.boardsList.isLoading;

export default selectIsBoardsListLoading;
