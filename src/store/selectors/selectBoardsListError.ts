import { TAppState } from '../TAppState';

const selectBoardsListError = (state: TAppState): string | null =>
  state.boardsList.error;

export default selectBoardsListError;
