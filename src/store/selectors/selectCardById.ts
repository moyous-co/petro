import { TAppState } from '../TAppState';
import { TCard } from '../../entities/card/TCard';

const selectCardById = (id: number) => (state: TAppState): TCard =>
  state.cards.itemsMap[id];

export default selectCardById;
