import { TAppState } from '../TAppState';
import { EPanelId } from '../../lib/consts';

const selectActivePanel = (state: TAppState): EPanelId =>
  state.general.activePanel;

export default selectActivePanel;
