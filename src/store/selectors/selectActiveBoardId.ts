import { TAppState } from '../TAppState';

const selectActiveBoardId = (state: TAppState): number =>
  state.activeBoard.retroId;

export default selectActiveBoardId;
