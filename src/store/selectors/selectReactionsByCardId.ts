import { TAppState } from '../TAppState';
import { TReaction } from '../../entities/reaction/TReaction';

const selectReactionsByCardId = (id: number) => (
  state: TAppState,
): TReaction[] => state.cards.itemsMap[id].reactions;

export default selectReactionsByCardId;
