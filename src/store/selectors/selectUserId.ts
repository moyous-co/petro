import { TAppState } from '../TAppState';

const selectUserId = (state: TAppState): number | null =>
  (state.user.isAuthorized && state.user.user?.id) || null;

export default selectUserId;
