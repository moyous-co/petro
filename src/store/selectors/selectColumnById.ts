import { TAppState } from '../TAppState';
import { TColumn } from '../../entities/column/TColumn';

const selectColumnById = (id: number) => (state: TAppState): TColumn =>
  state.columns.itemsMap[id];

export default selectColumnById;
