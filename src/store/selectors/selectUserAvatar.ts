import { TAppState } from '../TAppState';

const selectUserAvatar = (state: TAppState): string =>
  (state.user.isAuthorized && state.user.user?.avatar) || '';

export default selectUserAvatar;
