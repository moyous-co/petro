import { TAppState } from '../TAppState';
import { TMember } from '../../entities/member/TMember';

const selectMemberById = (id: number) => (state: TAppState): TMember =>
  state.members.itemsMap[id];

export default selectMemberById;
