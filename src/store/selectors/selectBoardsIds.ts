import { TAppState } from '../TAppState';

const selectBoardsIds = (state: TAppState): number[] =>
  state.boardsList.itemsIds;

export default selectBoardsIds;
