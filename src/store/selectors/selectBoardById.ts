import { TAppState } from '../TAppState';
import { TBoard } from '../../entities/board/TBoard';

const selectBoardById = (id: number) => (state: TAppState): TBoard =>
  state.boardsList.itemsMap[id];

export default selectBoardById;
