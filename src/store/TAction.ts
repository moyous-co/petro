import { setUser } from './actions/general/setUser';
import { setActivePanel } from './actions/general/setActivePanel';
import { fireInitialSetup } from './actions/general/fireInitialSetup';
import { setAccessToken } from './actions/general/setAccessToken';
import { fireCreateBoard } from './actions/board/createBoard/fireCreateBoard';
import { fireGetBoardsList } from './actions/board/getBoardsList/fireGetBoardsList';
import { TGetBoardsListAction } from './actions/board/getBoardsList/TGetBoardsListAction';
import { TCreateBoardAction } from './actions/board/createBoard/TCreateBoardAction';
import { TDeleteBoardAction } from './actions/board/deleteBoard/TDeleteBoardAction';
import { setActiveBoard } from './actions/board/setActiveBoard';
import { TGetColumnsAction } from './actions/columns/getColumns/TGetColumnsAction';
import { TGetCardsAction } from './actions/cards/getCards/TGetCardsAction';
import { TCreateColumnAction } from './actions/columns/createColumn/TCreateColumnAction';
import { TCreateCardAction } from './actions/cards/createCard/TCreateCardAction';
import { TEditColumnAction } from './actions/columns/editColumn/TEditColumnAction';
import { TDeleteColumnAction } from './actions/columns/deleteColumn/TDeleteColumnAction';
import { TEditCardAction } from './actions/cards/editCard/TEditCardAction';
import { TCreateReactionAction } from './actions/reactions/createReaction/TCreateReactionAction';
import { TDeleteReactionAction } from './actions/reactions/deleteReaction/TDeleteReactionAction';
import { TDeleteCardAction } from './actions/cards/deleteCard/TDeleteCardAction';
import { TCreateCommentAction } from './actions/comments/createComment/TCreateCommentAction';
import { TDeleteCommentAction } from './actions/comments/deleteComment/TDeleteCommentAction';
import { setMembers } from './actions/members/setMembers';
import { setProgress } from './actions/general/setProgress';

export type TAction =
  | TGetBoardsListAction
  | TCreateBoardAction
  | TDeleteBoardAction
  | TGetColumnsAction
  | TGetCardsAction
  | TCreateColumnAction
  | TCreateCardAction
  | TEditColumnAction
  | TDeleteColumnAction
  | TEditCardAction
  | TCreateReactionAction
  | TDeleteReactionAction
  | TDeleteCardAction
  | TCreateCommentAction
  | TDeleteCommentAction
  | ReturnType<
      | typeof setUser
      | typeof setActivePanel
      | typeof fireInitialSetup
      | typeof setAccessToken
      | typeof fireCreateBoard
      | typeof fireGetBoardsList
      | typeof setActiveBoard
      | typeof setMembers
      | typeof setProgress
    >;
