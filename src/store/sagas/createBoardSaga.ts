import { SagaIterator } from 'redux-saga';
import { call, put, select } from 'redux-saga/effects';
import { fireCreateBoard } from '../actions/board/createBoard/fireCreateBoard';
import selectAccessToken from '../selectors/selectAccessToken';
import commonFetch from '../../api/commonFetch';
import { TNewBoard } from '../../entities/board/TNewBoard';
import { TRawBoard } from '../../entities/board/TRawBoard';
import { TCommonResponse } from '../../api/TCommonResponse';
import { pendingCreateBoard } from '../actions/board/createBoard/pendingCreateBoard';
import { failureCreateBoard } from '../actions/board/createBoard/failureCreateBoard';
import { successCreateBoard } from '../actions/board/createBoard/successCreateBoard';
import { setActivePanel } from '../actions/general/setActivePanel';
import { EPanelId } from '../../lib/consts';
import { TRawMember } from '../../entities/member/TRawMember';
import { TRawCard } from '../../entities/card/TRawCard';
import selectUserId from '../selectors/selectUserId';
import getMembersSaga from './getMembersSaga';

function* createBoardSaga({
  payload,
}: ReturnType<typeof fireCreateBoard>): SagaIterator<void> {
  yield put(pendingCreateBoard());
  const accessToken = yield select(selectAccessToken);
  const response: TCommonResponse<TRawBoard> = yield call(() =>
    commonFetch<TRawBoard, TNewBoard>('createRetro', {
      ...payload,
      access_token: accessToken,
    }),
  );

  if (response.hasError) {
    yield put(failureCreateBoard(response.error));
    return;
  }

  const userId = yield select(selectUserId);

  if (userId && response.payload.id) {
    const memberResponse: TCommonResponse<{ items: TRawMember[] }> = yield call(
      () =>
        commonFetch<TRawCard, { retro_id: string; user_id: string }>(
          'createMember',
          {
            retro_id: String(response.payload.id),
            user_id: String(userId),
            access_token: accessToken,
          },
        ),
    );

    if (!memberResponse.hasError) {
      yield call(getMembersSaga, Number(response.payload.id));
    }
  }

  yield put(successCreateBoard(response.payload));
  yield put(setActivePanel(EPanelId.DASHBOARD));
}

export default createBoardSaga;
