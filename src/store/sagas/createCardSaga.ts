import { SagaIterator } from 'redux-saga';
import { call, put, select } from 'redux-saga/effects';
import { fireCreateCard } from '../actions/cards/createCard/fireCreateCard';
import selectAccessToken from '../selectors/selectAccessToken';
import commonFetch from '../../api/commonFetch';
import { TNewCard } from '../../entities/card/TNewCard';
import { TRawCard } from '../../entities/card/TRawCard';
import { TCommonResponse } from '../../api/TCommonResponse';
import { pendingCreateCard } from '../actions/cards/createCard/pendingCreateCard';
import { failureCreateCard } from '../actions/cards/createCard/failureCreateCard';
import { successCreateCard } from '../actions/cards/createCard/successCreateCard';

function* createCardSaga({
  payload,
}: ReturnType<typeof fireCreateCard>): SagaIterator<void> {
  yield put(pendingCreateCard());
  const accessToken = yield select(selectAccessToken);
  const response: TCommonResponse<TRawCard> = yield call(() =>
    commonFetch<TRawCard, TNewCard>('createCard', {
      ...payload,
      access_token: accessToken,
    }),
  );

  if (response.hasError) {
    yield put(failureCreateCard(response.error));
    return;
  }

  yield put(successCreateCard(response.payload));
}

export default createCardSaga;
