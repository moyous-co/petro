import { SagaIterator } from 'redux-saga';
import { call, put, select } from 'redux-saga/effects';
import { fireDeleteCard } from '../actions/cards/deleteCard/fireDeleteCard';
import selectAccessToken from '../selectors/selectAccessToken';
import commonFetch from '../../api/commonFetch';
import { TCommonResponse } from '../../api/TCommonResponse';
import { pendingDeleteCard } from '../actions/cards/deleteCard/pendingDeleteCard';
import { failureDeleteCard } from '../actions/cards/deleteCard/failureDeleteCard';
import { successDeleteCard } from '../actions/cards/deleteCard/successDeleteCard';
import { TCommonDeleteResponse } from '../../api/TCommonDeleteResponse';

function* deleteCardSaga({
  payload,
}: ReturnType<typeof fireDeleteCard>): SagaIterator<void> {
  yield put(pendingDeleteCard());
  const accessToken = yield select(selectAccessToken);
  const response: TCommonResponse<TCommonDeleteResponse> = yield call(() =>
    commonFetch<TCommonDeleteResponse, { id: string }>('deleteCard', {
      id: String(payload.id),
      access_token: accessToken,
    }),
  );

  if (response.hasError) {
    yield put(failureDeleteCard(response.error));
    return;
  }

  yield put(successDeleteCard({ id: payload.id, columnId: payload.column_id }));
}

export default deleteCardSaga;
