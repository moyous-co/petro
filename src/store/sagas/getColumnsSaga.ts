import { SagaIterator } from 'redux-saga';
import { call, put, select } from 'redux-saga/effects';
import { pendingGetColumns } from '../actions/columns/getColumns/pendingGetColumns';
import { successGetColumns } from '../actions/columns/getColumns/successGetColumns';
import selectAccessToken from '../selectors/selectAccessToken';
import commonFetch from '../../api/commonFetch';
import { TCommonResponse } from '../../api/TCommonResponse';
import { failureGetColumns } from '../actions/columns/getColumns/failureGetColumns';
import { TRawColumn } from '../../entities/column/TRawColumn';
import { fireGetColumns } from '../actions/columns/getColumns/fireGetColumns';

function* getColumnsSaga({
  payload,
}: ReturnType<typeof fireGetColumns>): SagaIterator<void> {
  const accessToken = yield select(selectAccessToken);
  yield put(pendingGetColumns());
  const response: TCommonResponse<{ items: TRawColumn[] }> = yield call(() =>
    commonFetch<TRawColumn, { retro_id: string }>('getColumns', {
      retro_id: String(payload),
      access_token: accessToken,
    }),
  );

  if (response.hasError) {
    yield put(failureGetColumns(response.error));
    return;
  }

  yield put(successGetColumns(response.payload.items));
}

export default getColumnsSaga;
