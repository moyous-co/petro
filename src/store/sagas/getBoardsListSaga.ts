import { SagaIterator } from 'redux-saga';
import { call, put, select } from 'redux-saga/effects';
import { pendingGetBoardsList } from '../actions/board/getBoardsList/pendingGetBoardsList';
import { successGetBoardsList } from '../actions/board/getBoardsList/successGetBoardsList';
import selectAccessToken from '../selectors/selectAccessToken';
import commonFetch from '../../api/commonFetch';
import { TRawBoard } from '../../entities/board/TRawBoard';
import { TCommonResponse } from '../../api/TCommonResponse';
import { failureGetBoardsList } from '../actions/board/getBoardsList/failureGetBoardsList';

function* getBoardsListSaga(): SagaIterator<void> {
  const accessToken = yield select(selectAccessToken);
  yield put(pendingGetBoardsList());
  const response: TCommonResponse<{ items: TRawBoard[] }> = yield call(() =>
    commonFetch<TRawBoard>('getRetros', {
      access_token: accessToken,
    }),
  );

  if (response.hasError) {
    yield put(failureGetBoardsList(response.error));
    return;
  }

  yield put(successGetBoardsList(response.payload.items));
}

export default getBoardsListSaga;
