import { SagaIterator } from 'redux-saga';
import { call, put, select } from 'redux-saga/effects';
import { fireDeleteComment } from '../actions/comments/deleteComment/fireDeleteComment';
import selectAccessToken from '../selectors/selectAccessToken';
import commonFetch from '../../api/commonFetch';
import { TCommonResponse } from '../../api/TCommonResponse';
import { pendingDeleteComment } from '../actions/comments/deleteComment/pendingDeleteComment';
import { failureDeleteComment } from '../actions/comments/deleteComment/failureDeleteComment';
import { successDeleteComment } from '../actions/comments/deleteComment/successDeleteComment';
import { TCommonDeleteResponse } from '../../api/TCommonDeleteResponse';

function* deleteCommentSaga({
  payload,
}: ReturnType<typeof fireDeleteComment>): SagaIterator<void> {
  yield put(pendingDeleteComment());
  const accessToken = yield select(selectAccessToken);
  const response: TCommonResponse<TCommonDeleteResponse> = yield call(() =>
    commonFetch<TCommonDeleteResponse, { id: string }>('deleteComment', {
      id: String(payload.id),
      access_token: accessToken,
    }),
  );

  if (response.hasError) {
    yield put(failureDeleteComment(response.error));
    return;
  }

  yield put(successDeleteComment({ id: payload.id, cardId: payload.cardId }));
}

export default deleteCommentSaga;
