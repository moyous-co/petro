import { SagaIterator } from 'redux-saga';
import { call, put, select } from 'redux-saga/effects';
import { fireDeleteBoard } from '../actions/board/deleteBoard/fireDeleteBoard';
import selectAccessToken from '../selectors/selectAccessToken';
import commonFetch from '../../api/commonFetch';
import { TCommonResponse } from '../../api/TCommonResponse';
import { pendingDeleteBoard } from '../actions/board/deleteBoard/pendingDeleteBoard';
import { failureDeleteBoard } from '../actions/board/deleteBoard/failureDeleteBoard';
import { successDeleteBoard } from '../actions/board/deleteBoard/successDeleteBoard';
import { setActivePanel } from '../actions/general/setActivePanel';
import { EPanelId } from '../../lib/consts';
import { fireGetBoardsList } from '../actions/board/getBoardsList/fireGetBoardsList';
import { TCommonDeleteResponse } from '../../api/TCommonDeleteResponse';

function* deleteBoardSaga({
  payload,
}: ReturnType<typeof fireDeleteBoard>): SagaIterator<void> {
  yield put(pendingDeleteBoard());
  const accessToken = yield select(selectAccessToken);
  const response: TCommonResponse<TCommonDeleteResponse> = yield call(() =>
    commonFetch<TCommonDeleteResponse, { id: string }>('deleteRetro', {
      id: String(payload.id),
      access_token: accessToken,
    }),
  );

  if (response.hasError) {
    yield put(failureDeleteBoard(response.error));
    return;
  }

  yield put(successDeleteBoard());
  yield put(setActivePanel(EPanelId.DASHBOARD));
  yield put(fireGetBoardsList());
}

export default deleteBoardSaga;
