import { SagaIterator } from 'redux-saga';
import { call, put, select } from 'redux-saga/effects';
import bridge from '@vkontakte/vk-bridge';
import { setActivePanel } from '../actions/general/setActivePanel';
import { APP_ID, EPanelId } from '../../lib/consts';
import { TRawUser } from '../../entities/user/TRawUser';
import { TUser } from '../../entities/user/TUser';
import cleanUser from '../../entities/user/cleanUser';
import { setUser } from '../actions/general/setUser';
import { TRawTokenInfo } from '../../entities/user/TRawTokenInfo';
import { setAccessToken } from '../actions/general/setAccessToken';
import { setProgress } from '../actions/general/setProgress';
import getParamFromHash from '../../lib/getParamFromHash';
import selectUserId from '../selectors/selectUserId';
import { TCommonResponse } from '../../api/TCommonResponse';
import { TRawMember } from '../../entities/member/TRawMember';
import commonFetch from '../../api/commonFetch';
import { TRawCard } from '../../entities/card/TRawCard';
import getMembersSaga from './getMembersSaga';
import selectAccessToken from '../selectors/selectAccessToken';
import openBoardSaga from './openBoardSaga';
import { fireOpenBoard } from '../actions/board/fireOpenBoard';
import getBoardsListSaga from './getBoardsListSaga';
import selectBoardById from '../selectors/selectBoardById';

function* initialSetupSaga(): SagaIterator<void> {
  bridge.subscribe(({ detail: { type, data } }) => {
    if (type === 'VKWebAppUpdateConfig') {
      const schemeAttribute = document.createAttribute('scheme');
      schemeAttribute.value = (data as any).scheme || 'client_light';
      document.body.attributes.setNamedItem(schemeAttribute);
    }
  });
  yield put(setProgress({ progress: 20, title: 'Загружаем данные по схеме' }));

  const tokenInfo: TRawTokenInfo = yield call(() =>
    bridge.send('VKWebAppGetAuthToken', {
      app_id: APP_ID,
      scope: 'friends',
    }),
  );
  yield put(setAccessToken(tokenInfo.access_token));
  yield put(setProgress({ progress: 30, title: 'Подтягиваем ключи доступа' }));

  const rawUser: TRawUser = yield call(() =>
    bridge.send('VKWebAppGetUserInfo'),
  );
  yield put(
    setProgress({ progress: 40, title: 'Загружаем информацию о пользователе' }),
  );
  const user: TUser = cleanUser(rawUser);
  yield put(setUser(user));
  yield put(
    setProgress({ progress: 55, title: 'Регистрируем текущего пользователя' }),
  );

  const retroId: string = getParamFromHash(window.location.hash, 'retro');
  const userId = yield select(selectUserId);
  const accessToken = yield select(selectAccessToken);

  if (userId && retroId) {
    yield put(setProgress({ progress: 75, title: 'Загружаем по приглашению' }));

    const memberResponse: TCommonResponse<{ items: TRawMember[] }> = yield call(
      () =>
        commonFetch<TRawCard, { retro_id: string; user_id: string }>(
          'createMember',
          {
            retro_id: String(retroId),
            user_id: String(userId),
            access_token: accessToken,
          },
        ),
    );

    if (!memberResponse.hasError) {
      yield call(getMembersSaga, Number(userId));
      yield call(getBoardsListSaga);

      const board = yield select(selectBoardById(Number(retroId)));

      if (board) {
        yield call(openBoardSaga, fireOpenBoard(Number(retroId)));
      }
    } else {
      yield call(getBoardsListSaga);
    }
  } else {
    yield call(getBoardsListSaga);
  }

  yield put(setActivePanel(EPanelId.DASHBOARD));
  window.location.hash = '';
}

export default initialSetupSaga;
