import { SagaIterator } from 'redux-saga';
import { call, put } from 'redux-saga/effects';
import { setActivePanel } from '../actions/general/setActivePanel';
import { EPanelId } from '../../lib/consts';
import { fireOpenBoard } from '../actions/board/fireOpenBoard';
import { setActiveBoard } from '../actions/board/setActiveBoard';
import { fireGetColumns } from '../actions/columns/getColumns/fireGetColumns';
import { fireGetCards } from '../actions/cards/getCards/fireGetCards';
import getColumnsSaga from './getColumnsSaga';
import getCardsSaga from './getCardsSaga';
import getMembersSaga from './getMembersSaga';

function* openBoardSaga({
  payload,
}: ReturnType<typeof fireOpenBoard>): SagaIterator<void> {
  yield put(setActiveBoard(payload));
  yield call(getColumnsSaga, fireGetColumns(payload));
  yield call(getCardsSaga, fireGetCards(payload));
  yield call(getMembersSaga, payload);
  yield put(setActivePanel(EPanelId.ACTIVE_BOARD));
}

export default openBoardSaga;
