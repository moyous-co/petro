import { SagaIterator } from 'redux-saga';
import { call, put, select } from 'redux-saga/effects';
import { fireEditCard } from '../actions/cards/editCard/fireEditCard';
import selectAccessToken from '../selectors/selectAccessToken';
import commonFetch from '../../api/commonFetch';
import { TRawCard } from '../../entities/card/TRawCard';
import { TCommonResponse } from '../../api/TCommonResponse';
import { pendingEditCard } from '../actions/cards/editCard/pendingEditCard';
import { failureEditCard } from '../actions/cards/editCard/failureEditCard';
import { successEditCard } from '../actions/cards/editCard/successEditCard';
import { TEditCard } from '../../entities/card/TEditCard';

function* editCardSaga({
  payload,
}: ReturnType<typeof fireEditCard>): SagaIterator<void> {
  yield put(pendingEditCard());
  const accessToken = yield select(selectAccessToken);
  const response: TCommonResponse<TRawCard> = yield call(() =>
    commonFetch<TRawCard, TEditCard>('updateCard', {
      ...payload,
      access_token: accessToken,
    }),
  );

  if (response.hasError) {
    yield put(failureEditCard(response.error));
    return;
  }

  yield put(successEditCard(response.payload));
}

export default editCardSaga;
