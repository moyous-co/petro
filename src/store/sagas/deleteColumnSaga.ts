import { SagaIterator } from 'redux-saga';
import { call, put, select } from 'redux-saga/effects';
import { fireDeleteColumn } from '../actions/columns/deleteColumn/fireDeleteColumn';
import selectAccessToken from '../selectors/selectAccessToken';
import commonFetch from '../../api/commonFetch';
import { TCommonResponse } from '../../api/TCommonResponse';
import { pendingDeleteColumn } from '../actions/columns/deleteColumn/pendingDeleteColumn';
import { failureDeleteColumn } from '../actions/columns/deleteColumn/failureDeleteColumn';
import { successDeleteColumn } from '../actions/columns/deleteColumn/successDeleteColumn';
import { TCommonDeleteResponse } from '../../api/TCommonDeleteResponse';

function* deleteColumnSaga({
  payload,
}: ReturnType<typeof fireDeleteColumn>): SagaIterator<void> {
  yield put(pendingDeleteColumn());
  const accessToken = yield select(selectAccessToken);
  const response: TCommonResponse<TCommonDeleteResponse> = yield call(() =>
    commonFetch<TCommonDeleteResponse, { id: string }>('deleteColumn', {
      id: String(payload.id),
      access_token: accessToken,
    }),
  );

  if (response.hasError) {
    yield put(failureDeleteColumn(response.error));
    return;
  }

  yield put(successDeleteColumn({ id: payload.id }));
}

export default deleteColumnSaga;
