import { SagaIterator } from 'redux-saga';
import bridge from '@vkontakte/vk-bridge';
import { call, select } from 'redux-saga/effects';
import { API_VERSION } from '../../lib/consts';
import selectAccessToken from '../selectors/selectAccessToken';
import { TCommonResponse } from '../../api/TCommonResponse';
import { TRawCard } from '../../entities/card/TRawCard';
import commonFetch from '../../api/commonFetch';
import { TRawMember } from '../../entities/member/TRawMember';

function* getMembersSaga(payload: number): SagaIterator<void> {
  const accessToken = yield select(selectAccessToken);

  const response: TCommonResponse<{ items: TRawMember[] }> = yield call(() =>
    commonFetch<TRawCard, { retro_id: string }>('getMembers', {
      retro_id: String(payload),
      access_token: accessToken,
    }),
  );

  if (response.hasError) {
    return;
  }

  if (response.payload.items.length > 0) {
    bridge.send('VKWebAppCallAPIMethod', {
      method: 'users.get',
      params: {
        user_ids: [response.payload.items.map(({ user_id }) => user_id)].join(
          ',',
        ),
        fields: 'photo_50',
        v: API_VERSION,
        access_token: accessToken,
      },
    });
  }
}

export default getMembersSaga;
