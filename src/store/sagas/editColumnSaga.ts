import { SagaIterator } from 'redux-saga';
import { call, put, select } from 'redux-saga/effects';
import { fireEditColumn } from '../actions/columns/editColumn/fireEditColumn';
import selectAccessToken from '../selectors/selectAccessToken';
import commonFetch from '../../api/commonFetch';
import { TRawColumn } from '../../entities/column/TRawColumn';
import { TCommonResponse } from '../../api/TCommonResponse';
import { pendingEditColumn } from '../actions/columns/editColumn/pendingEditColumn';
import { failureEditColumn } from '../actions/columns/editColumn/failureEditColumn';
import { successEditColumn } from '../actions/columns/editColumn/successEditColumn';
import { TEditColumn } from '../../entities/column/TEditColumn';

function* editColumnSaga({
  payload,
}: ReturnType<typeof fireEditColumn>): SagaIterator<void> {
  yield put(pendingEditColumn());
  const accessToken = yield select(selectAccessToken);
  const response: TCommonResponse<TRawColumn> = yield call(() =>
    commonFetch<TRawColumn, TEditColumn>('updateColumn', {
      ...payload,
      access_token: accessToken,
    }),
  );

  if (response.hasError) {
    yield put(failureEditColumn(response.error));
    return;
  }

  yield put(successEditColumn(response.payload));
}

export default editColumnSaga;
