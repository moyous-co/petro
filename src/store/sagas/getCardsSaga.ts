import { SagaIterator } from 'redux-saga';
import { call, put, select } from 'redux-saga/effects';
import { pendingGetCards } from '../actions/cards/getCards/pendingGetCards';
import { successGetCards } from '../actions/cards/getCards/successGetCards';
import selectAccessToken from '../selectors/selectAccessToken';
import commonFetch from '../../api/commonFetch';
import { TCommonResponse } from '../../api/TCommonResponse';
import { failureGetCards } from '../actions/cards/getCards/failureGetCards';
import { TRawCard } from '../../entities/card/TRawCard';
import { fireGetCards } from '../actions/cards/getCards/fireGetCards';

function* getCardsSaga({
  payload,
}: ReturnType<typeof fireGetCards>): SagaIterator<void> {
  const accessToken = yield select(selectAccessToken);
  yield put(pendingGetCards());
  const response: TCommonResponse<{ items: TRawCard[] }> = yield call(() =>
    commonFetch<TRawCard, { retro_id: string }>('getCards', {
      retro_id: String(payload),
      access_token: accessToken,
    }),
  );

  if (response.hasError) {
    yield put(failureGetCards(response.error));
    return;
  }

  yield put(successGetCards(response.payload.items));
}

export default getCardsSaga;
