import { SagaIterator } from 'redux-saga';
import { call, put, select } from 'redux-saga/effects';
import { fireCreateComment } from '../actions/comments/createComment/fireCreateComment';
import selectAccessToken from '../selectors/selectAccessToken';
import commonFetch from '../../api/commonFetch';
import { TNewComment } from '../../entities/comment/TNewComment';
import { TRawComment } from '../../entities/comment/TRawComment';
import { TCommonResponse } from '../../api/TCommonResponse';
import { pendingCreateComment } from '../actions/comments/createComment/pendingCreateComment';
import { failureCreateComment } from '../actions/comments/createComment/failureCreateComment';
import { successCreateComment } from '../actions/comments/createComment/successCreateComment';

function* createCommentSaga({
  payload,
}: ReturnType<typeof fireCreateComment>): SagaIterator<void> {
  yield put(pendingCreateComment());
  const accessToken = yield select(selectAccessToken);
  const response: TCommonResponse<TRawComment> = yield call(() =>
    commonFetch<TRawComment, TNewComment>('createComment', {
      ...payload,
      access_token: accessToken,
    }),
  );

  if (response.hasError) {
    yield put(failureCreateComment(response.error));
    return;
  }

  yield put(successCreateComment(response.payload));
}

export default createCommentSaga;
