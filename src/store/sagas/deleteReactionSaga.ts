import { SagaIterator } from 'redux-saga';
import { call, put, select } from 'redux-saga/effects';
import { fireDeleteReaction } from '../actions/reactions/deleteReaction/fireDeleteReaction';
import selectAccessToken from '../selectors/selectAccessToken';
import commonFetch from '../../api/commonFetch';
import { TCommonResponse } from '../../api/TCommonResponse';
import { pendingDeleteReaction } from '../actions/reactions/deleteReaction/pendingDeleteReaction';
import { failureDeleteReaction } from '../actions/reactions/deleteReaction/failureDeleteReaction';
import { successDeleteReaction } from '../actions/reactions/deleteReaction/successDeleteReaction';
import { TCommonDeleteResponse } from '../../api/TCommonDeleteResponse';

function* deleteReactionSaga({
  payload,
}: ReturnType<typeof fireDeleteReaction>): SagaIterator<void> {
  yield put(pendingDeleteReaction());
  const accessToken = yield select(selectAccessToken);
  const response: TCommonResponse<TCommonDeleteResponse> = yield call(() =>
    commonFetch<TCommonDeleteResponse, { id: string }>('deleteReaction', {
      id: String(payload.id),
      access_token: accessToken,
    }),
  );

  if (response.hasError) {
    yield put(failureDeleteReaction(response.error));
    return;
  }

  yield put(successDeleteReaction({ id: payload.id, cardId: payload.cardId }));
}

export default deleteReactionSaga;
