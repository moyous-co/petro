import { SagaIterator } from 'redux-saga';
import { call, put, select } from 'redux-saga/effects';
import { fireCreateReaction } from '../actions/reactions/createReaction/fireCreateReaction';
import selectAccessToken from '../selectors/selectAccessToken';
import commonFetch from '../../api/commonFetch';
import { TNewReaction } from '../../entities/reaction/TNewReaction';
import { TRawReaction } from '../../entities/reaction/TRawReaction';
import { TCommonResponse } from '../../api/TCommonResponse';
import { pendingCreateReaction } from '../actions/reactions/createReaction/pendingCreateReaction';
import { failureCreateReaction } from '../actions/reactions/createReaction/failureCreateReaction';
import { successCreateReaction } from '../actions/reactions/createReaction/successCreateReaction';

function* createReactionSaga({
  payload,
}: ReturnType<typeof fireCreateReaction>): SagaIterator<void> {
  yield put(pendingCreateReaction());
  const accessToken = yield select(selectAccessToken);
  const response: TCommonResponse<TRawReaction> = yield call(() =>
    commonFetch<TRawReaction, TNewReaction>('createReaction', {
      ...payload,
      access_token: accessToken,
    }),
  );

  if (response.hasError) {
    yield put(failureCreateReaction(response.error));
    return;
  }

  yield put(successCreateReaction(response.payload));
}

export default createReactionSaga;
