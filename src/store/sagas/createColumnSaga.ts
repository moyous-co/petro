import { SagaIterator } from 'redux-saga';
import { call, put, select } from 'redux-saga/effects';
import { fireCreateColumn } from '../actions/columns/createColumn/fireCreateColumn';
import selectAccessToken from '../selectors/selectAccessToken';
import commonFetch from '../../api/commonFetch';
import { TNewColumn } from '../../entities/column/TNewColumn';
import { TRawColumn } from '../../entities/column/TRawColumn';
import { TCommonResponse } from '../../api/TCommonResponse';
import { pendingCreateColumn } from '../actions/columns/createColumn/pendingCreateColumn';
import { failureCreateColumn } from '../actions/columns/createColumn/failureCreateColumn';
import { successCreateColumn } from '../actions/columns/createColumn/successCreateColumn';

function* createColumnSaga({
  payload,
}: ReturnType<typeof fireCreateColumn>): SagaIterator<void> {
  yield put(pendingCreateColumn());
  const accessToken = yield select(selectAccessToken);
  const response: TCommonResponse<TRawColumn> = yield call(() =>
    commonFetch<TRawColumn, TNewColumn>('createColumn', {
      ...payload,
      access_token: accessToken,
    }),
  );

  if (response.hasError) {
    yield put(failureCreateColumn(response.error));
    return;
  }

  yield put(successCreateColumn(response.payload));
}

export default createColumnSaga;
