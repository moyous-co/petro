import { composeWithDevTools } from 'redux-devtools-extension';
import { applyMiddleware, compose, createStore, Store } from 'redux';
import { useMemo } from 'react';
import createSagaMiddleware from 'redux-saga';
import rootReducer from './rootReducer';
import { isDevelopment } from '../lib/consts';
import appInitialState from './appInitialState';
import { TAppState } from './TAppState';
import rootSaga from './rootSaga';

const configureStore = (): Store<TAppState> => {
  const composeEnhancers = isDevelopment ? composeWithDevTools({}) : compose;
  const sagaMiddleware = createSagaMiddleware();
  const enhancer = composeEnhancers(applyMiddleware(sagaMiddleware));

  const store = createStore(rootReducer, appInitialState, enhancer);

  sagaMiddleware.run(rootSaga);

  return store;
};

const useStore = (): Store<TAppState> => useMemo(configureStore, []);

export default useStore;
