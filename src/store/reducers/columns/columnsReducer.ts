import { TColumnsState } from './TColumnsState';
import columnsInitialState from './columnsInitialState';
import { TAction } from '../../TAction';
import { SUCCESS_GET_CARDS } from '../../actions/cards/getCards/successGetCards';
import successGetCards from './handlers/getCards/successGetCards';
import { SUCCESS_CREATE_COLUMN } from '../../actions/columns/createColumn/successCreateColumn';
import successCreateColumn from './handlers/createColumn/successCreateColumn';
import { SUCCESS_CREATE_CARD } from '../../actions/cards/createCard/successCreateCard';
import successCreateCard from './handlers/createCard/successCreateCard';
import { SUCCESS_GET_COLUMNS } from '../../actions/columns/getColumns/successGetColumns';
import successGetColumns from './handlers/getColumns/successGetColumns';
import { SUCCESS_EDIT_COLUMN } from '../../actions/columns/editColumn/successEditColumn';
import successEditColumn from './handlers/editColumn/successEditColumn';
import { SUCCESS_DELETE_COLUMN } from '../../actions/columns/deleteColumn/successDeleteColumn';
import successDeleteColumn from './handlers/deleteColumn/successDeleteColumn';
import { SUCCESS_DELETE_CARD } from '../../actions/cards/deleteCard/successDeleteCard';
import successDeleteCard from './handlers/deleteCard/successDeleteCard';

const columnsReducer = (
  state: TColumnsState = columnsInitialState,
  action: TAction,
): TColumnsState => {
  switch (action.type) {
    case SUCCESS_GET_CARDS:
      return successGetCards(state, action);
    case SUCCESS_GET_COLUMNS:
      return successGetColumns(state, action);
    case SUCCESS_CREATE_COLUMN:
      return successCreateColumn(state, action);
    case SUCCESS_CREATE_CARD:
      return successCreateCard(state, action);
    case SUCCESS_EDIT_COLUMN:
      return successEditColumn(state, action);
    case SUCCESS_DELETE_COLUMN:
      return successDeleteColumn(state, action);
    case SUCCESS_DELETE_CARD:
      return successDeleteCard(state, action);
    default:
      return state;
  }
};

export default columnsReducer;
