import { TColumn } from '../../../entities/column/TColumn';

export type TColumnsState = {
  itemsMap: { [id: number]: TColumn };
};
