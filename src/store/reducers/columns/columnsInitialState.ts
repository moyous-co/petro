import { TColumnsState } from './TColumnsState';

const columnsInitialState: TColumnsState = {
  itemsMap: {},
};

export default columnsInitialState;
