import { TColumnsState } from '../../TColumnsState';
import { successEditColumn as successEditColumnAction } from '../../../../actions/columns/editColumn/successEditColumn';
import cleanColumn from '../../../../../entities/column/cleanColumn';

const successEditColumn = (
  state: TColumnsState,
  { payload: rawColumn }: ReturnType<typeof successEditColumnAction>,
): TColumnsState => {
  const column = cleanColumn(rawColumn);

  return {
    ...state,
    itemsMap: {
      ...state.itemsMap,
      [column.id]: { ...column, cardsIds: state.itemsMap[column.id].cardsIds },
    },
  };
};

export default successEditColumn;
