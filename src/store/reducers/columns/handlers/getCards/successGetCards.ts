import { successGetCards as successGetCardsAction } from '../../../../actions/cards/getCards/successGetCards';
import { TColumnsState } from '../../TColumnsState';

const successGetCards = (
  state: TColumnsState,
  { payload }: ReturnType<typeof successGetCardsAction>,
): TColumnsState => {
  const nextItemsMap = state.itemsMap;

  payload.forEach(({ id: rawId, column_id: rawColumnId }) => {
    const columnId = Number(rawColumnId);
    const id = Number(rawId);

    if (nextItemsMap[columnId]) {
      nextItemsMap[columnId] = {
        ...nextItemsMap[columnId],
        cardsIds: [...nextItemsMap[columnId].cardsIds, id],
      };
    }
  });

  return {
    ...state,
    itemsMap: nextItemsMap,
  };
};

export default successGetCards;
