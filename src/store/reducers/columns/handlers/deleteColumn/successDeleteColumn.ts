import { TColumnsState } from '../../TColumnsState';
import { successDeleteColumn as successDeleteColumnAction } from '../../../../actions/columns/deleteColumn/successDeleteColumn';

const successDeleteColumn = (
  state: TColumnsState,
  { payload }: ReturnType<typeof successDeleteColumnAction>,
): TColumnsState => {
  const { itemsMap } = state;

  delete itemsMap[payload.id];

  return {
    ...state,
    itemsMap,
  };
};

export default successDeleteColumn;
