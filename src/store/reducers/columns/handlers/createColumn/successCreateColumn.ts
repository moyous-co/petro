import { TColumnsState } from '../../TColumnsState';
import { successCreateColumn as successCreateColumnAction } from '../../../../actions/columns/createColumn/successCreateColumn';
import cleanColumn from '../../../../../entities/column/cleanColumn';

const successCreateColumn = (
  state: TColumnsState,
  { payload: rawColumn }: ReturnType<typeof successCreateColumnAction>,
): TColumnsState => {
  const column = cleanColumn(rawColumn);

  return {
    ...state,
    itemsMap: {
      ...state.itemsMap,
      [column.id]: column,
    },
  };
};

export default successCreateColumn;
