import { TColumnsState } from '../../TColumnsState';
import { successCreateCard as successCreateCardAction } from '../../../../actions/cards/createCard/successCreateCard';

const successCreateCard = (
  state: TColumnsState,
  {
    payload: { id: rawId, column_id: rawColumnId },
  }: ReturnType<typeof successCreateCardAction>,
): TColumnsState => {
  const id = Number(rawId);
  const columnId = Number(rawColumnId);

  return {
    ...state,
    itemsMap: {
      ...state.itemsMap,
      [columnId]: {
        ...state.itemsMap[columnId],
        cardsIds: [...state.itemsMap[columnId].cardsIds, id],
      },
    },
  };
};

export default successCreateCard;
