import { successGetColumns as successGetColumnsAction } from '../../../../actions/columns/getColumns/successGetColumns';
import { TColumnsState } from '../../TColumnsState';
import cleanColumn from '../../../../../entities/column/cleanColumn';
import { TColumn } from '../../../../../entities/column/TColumn';

const successGetColumns = (
  state: TColumnsState,
  { payload }: ReturnType<typeof successGetColumnsAction>,
): TColumnsState => ({
  ...state,
  itemsMap: payload
    .map((rawColumn) => cleanColumn(rawColumn))
    .reduce<{ [id: number]: TColumn }>((acc, cur) => {
      acc[cur.id] = cur;

      return acc;
    }, {}),
});

export default successGetColumns;
