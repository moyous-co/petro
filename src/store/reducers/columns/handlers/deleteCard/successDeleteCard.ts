import { TColumnsState } from '../../TColumnsState';
import { successDeleteCard as successDeleteCardAction } from '../../../../actions/cards/deleteCard/successDeleteCard';

const successDeleteCard = (
  state: TColumnsState,
  {
    payload: { id: rawId, columnId },
  }: ReturnType<typeof successDeleteCardAction>,
): TColumnsState => {
  const id = Number(rawId);

  return {
    ...state,
    itemsMap: {
      ...state.itemsMap,
      [columnId]: {
        ...state.itemsMap[columnId],
        cardsIds: state.itemsMap[columnId].cardsIds.filter(
          (cardId) => cardId !== id,
        ),
      },
    },
  };
};

export default successDeleteCard;
