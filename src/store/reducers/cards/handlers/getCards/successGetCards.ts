import { successGetCards as successGetCardsAction } from '../../../../actions/cards/getCards/successGetCards';
import { TCardsState } from '../../TCardsState';
import cleanCard from '../../../../../entities/card/cleanCard';
import { TCard } from '../../../../../entities/card/TCard';

const successGetCards = (
  state: TCardsState,
  { payload }: ReturnType<typeof successGetCardsAction>,
): TCardsState => ({
  ...state,
  itemsMap: payload
    .map((rawCard) => cleanCard(rawCard))
    .reduce<{ [id: number]: TCard }>((acc, cur) => {
      acc[cur.id] = cur;

      return acc;
    }, {}),
});

export default successGetCards;
