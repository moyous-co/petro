import { TCardsState } from '../../TCardsState';
import { successCreateComment as successCreateCommentAction } from '../../../../actions/comments/createComment/successCreateComment';
import cleanComment from '../../../../../entities/comment/cleanComment';

const successCreateComment = (
  state: TCardsState,
  { payload: rawComment }: ReturnType<typeof successCreateCommentAction>,
): TCardsState => {
  const comment = cleanComment(rawComment);

  return {
    ...state,
    itemsMap: {
      ...state.itemsMap,
      [comment.cardId]: {
        ...state.itemsMap[comment.cardId],
        comments: [...state.itemsMap[comment.cardId].comments, comment],
      },
    },
  };
};

export default successCreateComment;
