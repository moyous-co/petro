import { TCardsState } from '../../TCardsState';
import { successDeleteComment as successDeleteCommentAction } from '../../../../actions/comments/deleteComment/successDeleteComment';

const successDeleteComment = (
  state: TCardsState,
  { payload }: ReturnType<typeof successDeleteCommentAction>,
): TCardsState => {
  return {
    ...state,
    itemsMap: {
      ...state.itemsMap,
      [payload.cardId]: {
        ...state.itemsMap[payload.cardId],
        comments: state.itemsMap[payload.cardId].comments.filter(
          ({ id }) => id !== payload.id,
        ),
      },
    },
  };
};

export default successDeleteComment;
