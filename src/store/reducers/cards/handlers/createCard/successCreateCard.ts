import { TCardsState } from '../../TCardsState';
import { successCreateCard as successCreateCardAction } from '../../../../actions/cards/createCard/successCreateCard';
import cleanCard from '../../../../../entities/card/cleanCard';

const successCreateCard = (
  state: TCardsState,
  { payload: rawCard }: ReturnType<typeof successCreateCardAction>,
): TCardsState => {
  const card = cleanCard(rawCard);

  return {
    ...state,
    itemsMap: {
      ...state.itemsMap,
      [card.id]: card,
    },
  };
};

export default successCreateCard;
