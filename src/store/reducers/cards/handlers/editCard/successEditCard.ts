import { TCardsState } from '../../TCardsState';
import { successEditCard as successEditCardAction } from '../../../../actions/cards/editCard/successEditCard';
import cleanCard from '../../../../../entities/card/cleanCard';

const successEditCard = (
  state: TCardsState,
  { payload: rawCard }: ReturnType<typeof successEditCardAction>,
): TCardsState => {
  const card = cleanCard(rawCard);

  return {
    ...state,
    itemsMap: {
      ...state.itemsMap,
      [card.id]: {
        ...card,
        reactions: state.itemsMap[card.id].reactions,
        comments: state.itemsMap[card.id].comments,
      },
    },
  };
};

export default successEditCard;
