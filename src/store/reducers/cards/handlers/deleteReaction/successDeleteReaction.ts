import { TCardsState } from '../../TCardsState';
import { successDeleteReaction as successDeleteReactionAction } from '../../../../actions/reactions/deleteReaction/successDeleteReaction';

const successDeleteReaction = (
  state: TCardsState,
  { payload }: ReturnType<typeof successDeleteReactionAction>,
): TCardsState => {
  return {
    ...state,
    itemsMap: {
      ...state.itemsMap,
      [payload.cardId]: {
        ...state.itemsMap[payload.cardId],
        reactions: state.itemsMap[payload.cardId].reactions.filter(
          ({ id }) => id !== payload.id,
        ),
      },
    },
  };
};

export default successDeleteReaction;
