import { TCardsState } from '../../TCardsState';
import { successCreateReaction as successCreateReactionAction } from '../../../../actions/reactions/createReaction/successCreateReaction';
import cleanReaction from '../../../../../entities/reaction/cleanReaction';

const successCreateReaction = (
  state: TCardsState,
  { payload: rawReaction }: ReturnType<typeof successCreateReactionAction>,
): TCardsState => {
  const reaction = cleanReaction(rawReaction);

  return {
    ...state,
    itemsMap: {
      ...state.itemsMap,
      [reaction.cardId]: {
        ...state.itemsMap[reaction.cardId],
        reactions: [...state.itemsMap[reaction.cardId].reactions, reaction],
      },
    },
  };
};

export default successCreateReaction;
