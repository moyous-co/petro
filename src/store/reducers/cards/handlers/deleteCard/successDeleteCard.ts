import { TCardsState } from '../../TCardsState';
import { successDeleteCard as successDeleteCardAction } from '../../../../actions/cards/deleteCard/successDeleteCard';

const successDeleteCard = (
  state: TCardsState,
  { payload }: ReturnType<typeof successDeleteCardAction>,
): TCardsState => {
  const { itemsMap } = state;

  delete itemsMap[payload.id];

  return {
    ...state,
    itemsMap,
  };
};

export default successDeleteCard;
