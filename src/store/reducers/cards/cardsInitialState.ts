import { TCardsState } from './TCardsState';

const cardsInitialState: TCardsState = {
  itemsMap: {},
};

export default cardsInitialState;
