import { TCard } from '../../../entities/card/TCard';

export type TCardsState = {
  itemsMap: { [id: number]: TCard };
};
