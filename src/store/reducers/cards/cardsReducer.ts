import { TCardsState } from './TCardsState';
import { TAction } from '../../TAction';
import cardsInitialState from './cardsInitialState';
import { SUCCESS_CREATE_CARD } from '../../actions/cards/createCard/successCreateCard';
import successCreateCard from './handlers/createCard/successCreateCard';
import { SUCCESS_GET_CARDS } from '../../actions/cards/getCards/successGetCards';
import successGetCards from './handlers/getCards/successGetCards';
import { SUCCESS_EDIT_CARD } from '../../actions/cards/editCard/successEditCard';
import successEditCard from './handlers/editCard/successEditCard';
import { SUCCESS_CREATE_REACTION } from '../../actions/reactions/createReaction/successCreateReaction';
import successCreateReaction from './handlers/createReaction/successCreateReaction';
import { SUCCESS_DELETE_REACTION } from '../../actions/reactions/deleteReaction/successDeleteReaction';
import successDeleteReaction from './handlers/deleteReaction/successDeleteReaction';
import { SUCCESS_DELETE_CARD } from '../../actions/cards/deleteCard/successDeleteCard';
import successDeleteCard from './handlers/deleteCard/successDeleteCard';
import { SUCCESS_CREATE_COMMENT } from '../../actions/comments/createComment/successCreateComment';
import successCreateComment from './handlers/createComment/successCreateComment';
import { SUCCESS_DELETE_COMMENT } from '../../actions/comments/deleteComment/successDeleteComment';
import successDeleteComment from './handlers/deleteComment/successDeleteComment';

const cardsReducer = (
  state: TCardsState = cardsInitialState,
  action: TAction,
): TCardsState => {
  switch (action.type) {
    case SUCCESS_GET_CARDS:
      return successGetCards(state, action);
    case SUCCESS_CREATE_CARD:
      return successCreateCard(state, action);
    case SUCCESS_EDIT_CARD:
      return successEditCard(state, action);
    case SUCCESS_CREATE_REACTION:
      return successCreateReaction(state, action);
    case SUCCESS_DELETE_REACTION:
      return successDeleteReaction(state, action);
    case SUCCESS_DELETE_CARD:
      return successDeleteCard(state, action);
    case SUCCESS_CREATE_COMMENT:
      return successCreateComment(state, action);
    case SUCCESS_DELETE_COMMENT:
      return successDeleteComment(state, action);
    default:
      return state;
  }
};

export default cardsReducer;
