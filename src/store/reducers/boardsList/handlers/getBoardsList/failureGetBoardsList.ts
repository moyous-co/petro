import { failureGetBoardsList as failureGetBoardsListAction } from '../../../../actions/board/getBoardsList/failureGetBoardsList';
import { TBoardsListState } from '../../TBoardsListState';

const failureGetBoardsList = (
  state: TBoardsListState,
  { payload }: ReturnType<typeof failureGetBoardsListAction>,
): TBoardsListState => ({
  ...state,
  isLoading: false,
  isLoaded: true,
  error: payload,
});

export default failureGetBoardsList;
