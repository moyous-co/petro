import { TBoardsListState } from '../../TBoardsListState';

const pendingGetBoardsList = (state: TBoardsListState): TBoardsListState => ({
  ...state,
  isLoading: true,
});

export default pendingGetBoardsList;
