import { successGetBoardsList as successGetBoardsListAction } from '../../../../actions/board/getBoardsList/successGetBoardsList';
import { TBoardsListState } from '../../TBoardsListState';
import cleanBoard from '../../../../../entities/board/cleanBoard';
import { TBoard } from '../../../../../entities/board/TBoard';

const successGetBoardsList = (
  state: TBoardsListState,
  { payload }: ReturnType<typeof successGetBoardsListAction>,
): TBoardsListState => ({
  ...state,
  itemsIds: payload.map(({ id }) => Number(id)),
  itemsMap: payload
    .map((rawBoard) => cleanBoard(rawBoard))
    .reduce<{ [id: number]: TBoard }>((acc, cur) => {
      acc[cur.id] = cur;

      return acc;
    }, {}),
  isLoading: false,
  isLoaded: true,
});

export default successGetBoardsList;
