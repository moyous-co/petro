import { TBoardsListState } from '../../TBoardsListState';
import { successCreateBoard as successCreateBoardAction } from '../../../../actions/board/createBoard/successCreateBoard';
import cleanBoard from '../../../../../entities/board/cleanBoard';

const successCreateBoard = (
  state: TBoardsListState,
  { payload: rawBoard }: ReturnType<typeof successCreateBoardAction>,
): TBoardsListState => {
  const board = cleanBoard(rawBoard);

  return {
    ...state,
    itemsIds: [...state.itemsIds, board.id],
    itemsMap: {
      ...state.itemsMap,
      [board.id]: board,
    },
  };
};

export default successCreateBoard;
