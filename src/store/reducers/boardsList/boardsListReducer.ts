import { TBoardsListState } from './TBoardsListState';
import boardsListInitialState from './boardsListInitialState';
import { TAction } from '../../TAction';
import { PENDING_GET_BOARDS_LIST } from '../../actions/board/getBoardsList/pendingGetBoardsList';
import pendingGetBoardsList from './handlers/getBoardsList/pendingGetBoardsList';
import { SUCCESS_GET_BOARDS_LIST } from '../../actions/board/getBoardsList/successGetBoardsList';
import successGetBoardsList from './handlers/getBoardsList/successGetBoardsList';
import { FAILURE_GET_BOARDS_LIST } from '../../actions/board/getBoardsList/failureGetBoardsList';
import failureGetBoardsList from './handlers/getBoardsList/failureGetBoardsList';
import { SUCCESS_CREATE_BOARD } from '../../actions/board/createBoard/successCreateBoard';
import successCreateBoard from './handlers/createBoard/successCreateBoard';

const boardsListReducer = (
  state: TBoardsListState = boardsListInitialState,
  action: TAction,
): TBoardsListState => {
  switch (action.type) {
    case PENDING_GET_BOARDS_LIST:
      return pendingGetBoardsList(state);
    case SUCCESS_GET_BOARDS_LIST:
      return successGetBoardsList(state, action);
    case FAILURE_GET_BOARDS_LIST:
      return failureGetBoardsList(state, action);
    case SUCCESS_CREATE_BOARD:
      return successCreateBoard(state, action);
    default:
      return state;
  }
};

export default boardsListReducer;
