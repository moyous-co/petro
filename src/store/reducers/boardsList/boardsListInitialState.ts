import { TBoardsListState } from './TBoardsListState';

const boardsListInitialState: TBoardsListState = {
  itemsIds: [],
  itemsMap: {},
  isLoaded: false,
  isLoading: false,
  error: null,
};

export default boardsListInitialState;
