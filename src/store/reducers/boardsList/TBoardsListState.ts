import { TBoard } from '../../../entities/board/TBoard';

export type TBoardsListState = {
  itemsIds: number[];
  itemsMap: { [id: number]: TBoard };
  isLoaded: boolean;
  isLoading: boolean;
  error: string | null;
};
