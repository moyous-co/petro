import { TReaction } from '../../../entities/reaction/TReaction';

export type TReactionsState = {
  itemsMap: { [id: number]: TReaction };
};
