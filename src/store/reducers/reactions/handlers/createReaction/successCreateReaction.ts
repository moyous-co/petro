import { TReactionsState } from '../../TReactionsState';
import { successCreateReaction as successCreateReactionAction } from '../../../../actions/reactions/createReaction/successCreateReaction';
import cleanReaction from '../../../../../entities/reaction/cleanReaction';

const successCreateReaction = (
  state: TReactionsState,
  { payload: rawReaction }: ReturnType<typeof successCreateReactionAction>,
): TReactionsState => {
  const reaction = cleanReaction(rawReaction);

  return {
    ...state,
    itemsMap: {
      ...state.itemsMap,
      [reaction.id]: reaction,
    },
  };
};

export default successCreateReaction;
