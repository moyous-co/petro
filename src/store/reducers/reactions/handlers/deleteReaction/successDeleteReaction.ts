import { TReactionsState } from '../../TReactionsState';
import { successDeleteReaction as successDeleteReactionAction } from '../../../../actions/reactions/deleteReaction/successDeleteReaction';

const successDeleteReaction = (
  state: TReactionsState,
  { payload }: ReturnType<typeof successDeleteReactionAction>,
): TReactionsState => {
  const { itemsMap } = state;

  delete itemsMap[payload.id];

  return {
    ...state,
    itemsMap,
  };
};

export default successDeleteReaction;
