import { TReactionsState } from './TReactionsState';
import { TAction } from '../../TAction';
import reactionsInitialState from './reactionsInitialState';
import { SUCCESS_CREATE_REACTION } from '../../actions/reactions/createReaction/successCreateReaction';
import successCreateReaction from './handlers/createReaction/successCreateReaction';
import { SUCCESS_DELETE_REACTION } from '../../actions/reactions/deleteReaction/successDeleteReaction';
import successDeleteReaction from './handlers/deleteReaction/successDeleteReaction';

const reactionsReducer = (
  state: TReactionsState = reactionsInitialState,
  action: TAction,
): TReactionsState => {
  switch (action.type) {
    case SUCCESS_CREATE_REACTION:
      return successCreateReaction(state, action);
    case SUCCESS_DELETE_REACTION:
      return successDeleteReaction(state, action);
    default:
      return state;
  }
};

export default reactionsReducer;
