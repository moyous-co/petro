import { TReactionsState } from './TReactionsState';

const reactionsInitialState: TReactionsState = {
  itemsMap: {},
};

export default reactionsInitialState;
