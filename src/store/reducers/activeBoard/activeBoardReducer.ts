import { TActiveBoardState } from './TActiveBoardState';
import { TAction } from '../../TAction';
import activeBoardInitialState from './activeBoardInitialState';
import { SET_ACTIVE_BOARD } from '../../actions/board/setActiveBoard';
import setActiveBoard from './handlers/setActiveBoard';
import { SUCCESS_GET_COLUMNS } from '../../actions/columns/getColumns/successGetColumns';
import successGetColumns from './handlers/successGetColumns';
import { SUCCESS_CREATE_COLUMN } from '../../actions/columns/createColumn/successCreateColumn';
import successCreateColumn from './handlers/createColumn/successCreateColumn';
import { SUCCESS_DELETE_COLUMN } from '../../actions/columns/deleteColumn/successDeleteColumn';
import successDeleteColumn from './handlers/deleteColumn/successDeleteColumn';

const activeBoardReducer = (
  state: TActiveBoardState = activeBoardInitialState,
  action: TAction,
): TActiveBoardState => {
  switch (action.type) {
    case SET_ACTIVE_BOARD:
      return setActiveBoard(state, action);
    case SUCCESS_GET_COLUMNS:
      return successGetColumns(state, action);
    case SUCCESS_CREATE_COLUMN:
      return successCreateColumn(state, action);
    case SUCCESS_DELETE_COLUMN:
      return successDeleteColumn(state, action);
    default:
      return state;
  }
};

export default activeBoardReducer;
