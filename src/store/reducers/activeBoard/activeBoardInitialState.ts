import { TActiveBoardState } from './TActiveBoardState';

const activeBoardInitialState: TActiveBoardState = {
  retroId: -1,
  columnIds: [],
  lastUpdateTime: null,
};

export default activeBoardInitialState;
