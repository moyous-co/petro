export type TActiveBoardState = {
  retroId: number;
  columnIds: number[];
  lastUpdateTime: number | null;
};
