import { TActiveBoardState } from '../../TActiveBoardState';
import { successDeleteColumn as successDeleteColumnAction } from '../../../../actions/columns/deleteColumn/successDeleteColumn';

const successDeleteColumn = (
  state: TActiveBoardState,
  { payload }: ReturnType<typeof successDeleteColumnAction>,
): TActiveBoardState => ({
  ...state,
  columnIds: state.columnIds.filter((id) => payload.id !== id),
});

export default successDeleteColumn;
