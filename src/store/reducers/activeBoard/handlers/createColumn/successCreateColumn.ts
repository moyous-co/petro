import { TActiveBoardState } from '../../TActiveBoardState';
import { successCreateColumn as successCreateColumnAction } from '../../../../actions/columns/createColumn/successCreateColumn';

const successCreateColumn = (
  state: TActiveBoardState,
  { payload: rawColumn }: ReturnType<typeof successCreateColumnAction>,
): TActiveBoardState => ({
  ...state,
  columnIds: [...state.columnIds, Number(rawColumn.id)],
});

export default successCreateColumn;
