import { successGetColumns as successGetColumnsAction } from '../../../actions/columns/getColumns/successGetColumns';
import { TActiveBoardState } from '../TActiveBoardState';

const successGetColumns = (
  state: TActiveBoardState,
  { payload }: ReturnType<typeof successGetColumnsAction>,
): TActiveBoardState => ({
  ...state,
  columnIds: payload.map(({ id }) => Number(id)),
});

export default successGetColumns;
