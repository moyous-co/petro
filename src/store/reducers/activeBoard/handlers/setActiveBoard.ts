import { TActiveBoardState } from '../TActiveBoardState';
import { setActiveBoard as setActiveBoardAction } from '../../../actions/board/setActiveBoard';

const setActiveBoard = (
  state: TActiveBoardState,
  { payload }: ReturnType<typeof setActiveBoardAction>,
): TActiveBoardState => ({
  ...state,
  retroId: payload,
});

export default setActiveBoard;
