import { TMembersState } from './TMembersState';
import membersInitialState from './membersInitialState';
import { TAction } from '../../TAction';
import { SET_MEMBERS } from '../../actions/members/setMembers';
import setMembers from './handlers/setMembers';

const membersReducer = (
  state: TMembersState = membersInitialState,
  action: TAction,
): TMembersState => {
  switch (action.type) {
    case SET_MEMBERS:
      return setMembers(state, action);
    default:
      return state;
  }
};

export default membersReducer;
