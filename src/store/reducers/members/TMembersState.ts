import { TMember } from '../../../entities/member/TMember';

export type TMembersState = {
  itemsIds: number[];
  itemsMap: { [id: number]: TMember };
};
