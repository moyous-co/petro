import { TMembersState } from './TMembersState';

const membersInitialState: TMembersState = {
  itemsIds: [],
  itemsMap: {},
};

export default membersInitialState;
