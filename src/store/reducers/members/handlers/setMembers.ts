import { setMembers as setMembersAction } from '../../../actions/members/setMembers';
import { TMembersState } from '../TMembersState';
import { TMember } from '../../../../entities/member/TMember';

const setMembers = (
  state: TMembersState,
  { payload }: ReturnType<typeof setMembersAction>,
): TMembersState => ({
  ...state,
  itemsIds: payload.map(({ id }) => id),
  itemsMap: payload.reduce<{ [id: number]: TMember }>((acc, cur) => {
    acc[cur.id] = cur;

    return acc;
  }, {}),
});

export default setMembers;
