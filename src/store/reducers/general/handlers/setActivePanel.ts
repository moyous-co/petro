import { setActivePanel as setActivePanelAction } from '../../../actions/general/setActivePanel';
import { TGeneralState } from '../TGeneralState';

const setActivePanel = (
  state: TGeneralState,
  { payload }: ReturnType<typeof setActivePanelAction>,
): TGeneralState => ({ ...state, activePanel: payload });

export default setActivePanel;
