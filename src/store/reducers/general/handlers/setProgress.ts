import { setProgress as setProgressAction } from '../../../actions/general/setProgress';
import { TGeneralState } from '../TGeneralState';

const setProgress = (
  state: TGeneralState,
  { payload: { progress, title } }: ReturnType<typeof setProgressAction>,
): TGeneralState => ({ ...state, progress, progressTitle: title });

export default setProgress;
