import { TGeneralState } from './TGeneralState';
import { EPanelId } from '../../../lib/consts';

const generalInitialState: TGeneralState = {
  activePanel: EPanelId.INITIAL,
  progress: 10,
  progressTitle: 'Начинаем подготовку приложения',
};

export default generalInitialState;
