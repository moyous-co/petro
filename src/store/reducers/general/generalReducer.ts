import { TGeneralState } from './TGeneralState';
import { TAction } from '../../TAction';
import generalInitialState from './generalInitialState';
import { SET_ACTIVE_PANEL } from '../../actions/general/setActivePanel';
import setActivePanel from './handlers/setActivePanel';
import { SET_PROGRESS } from '../../actions/general/setProgress';
import setProgress from './handlers/setProgress';

const generalReducer = (
  state: TGeneralState = generalInitialState,
  action: TAction,
): TGeneralState => {
  switch (action.type) {
    case SET_ACTIVE_PANEL:
      return setActivePanel(state, action);
    case SET_PROGRESS:
      return setProgress(state, action);
    default:
      return state;
  }
};

export default generalReducer;
