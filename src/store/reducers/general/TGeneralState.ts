import { EPanelId } from '../../../lib/consts';

export type TGeneralState = {
  activePanel: EPanelId;
  progress: number;
  progressTitle: string;
};
