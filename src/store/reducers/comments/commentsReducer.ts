import { TCommentsState } from './TCommentsState';
import { TAction } from '../../TAction';
import commentsInitialState from './commentsInitialState';
import { SUCCESS_CREATE_COMMENT } from '../../actions/comments/createComment/successCreateComment';
import successCreateComment from './handlers/createComment/successCreateComment';
import { SUCCESS_DELETE_COMMENT } from '../../actions/comments/deleteComment/successDeleteComment';
import successDeleteComment from './handlers/deleteComment/successDeleteComment';

const commentsReducer = (
  state: TCommentsState = commentsInitialState,
  action: TAction,
): TCommentsState => {
  switch (action.type) {
    case SUCCESS_CREATE_COMMENT:
      return successCreateComment(state, action);
    case SUCCESS_DELETE_COMMENT:
      return successDeleteComment(state, action);
    default:
      return state;
  }
};

export default commentsReducer;
