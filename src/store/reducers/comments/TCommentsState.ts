import { TComment } from '../../../entities/comment/TComment';

export type TCommentsState = {
  itemsMap: { [id: number]: TComment };
};
