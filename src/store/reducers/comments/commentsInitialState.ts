import { TCommentsState } from './TCommentsState';

const commentsInitialState: TCommentsState = {
  itemsMap: {},
};

export default commentsInitialState;
