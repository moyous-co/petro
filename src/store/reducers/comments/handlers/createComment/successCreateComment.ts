import { TCommentsState } from '../../TCommentsState';
import { successCreateComment as successCreateCommentAction } from '../../../../actions/comments/createComment/successCreateComment';
import cleanComment from '../../../../../entities/comment/cleanComment';

const successCreateComment = (
  state: TCommentsState,
  { payload: rawComment }: ReturnType<typeof successCreateCommentAction>,
): TCommentsState => {
  const reaction = cleanComment(rawComment);

  return {
    ...state,
    itemsMap: {
      ...state.itemsMap,
      [reaction.id]: reaction,
    },
  };
};

export default successCreateComment;
