import { TCommentsState } from '../../TCommentsState';
import { successDeleteComment as successDeleteCommentAction } from '../../../../actions/comments/deleteComment/successDeleteComment';

const successDeleteComment = (
  state: TCommentsState,
  { payload }: ReturnType<typeof successDeleteCommentAction>,
): TCommentsState => {
  const { itemsMap } = state;

  delete itemsMap[payload.id];

  return {
    ...state,
    itemsMap,
  };
};

export default successDeleteComment;
