import { setUser as setUserAction } from '../../../actions/general/setUser';
import { TUserState } from '../TUserState';

const setUser = (
  state: TUserState,
  { payload }: ReturnType<typeof setUserAction>,
): TUserState => ({ ...state, user: payload, isAuthorized: true });

export default setUser;
