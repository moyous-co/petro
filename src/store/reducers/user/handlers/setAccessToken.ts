import { setAccessToken as setAccessTokenAction } from '../../../actions/general/setAccessToken';
import { TUserState } from '../TUserState';

const setAccessToken = (
  state: TUserState,
  { payload }: ReturnType<typeof setAccessTokenAction>,
): TUserState => ({ ...state, accessToken: payload });

export default setAccessToken;
