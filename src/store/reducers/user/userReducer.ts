import { TUserState } from './TUserState';
import { TAction } from '../../TAction';
import userInitialState from './userInitialState';
import { SET_USER } from '../../actions/general/setUser';
import setUser from './handlers/setUser';
import { SET_ACCESS_TOKEN } from '../../actions/general/setAccessToken';
import setAccessToken from './handlers/setAccessToken';

const userReducer = (
  state: TUserState = userInitialState,
  action: TAction,
): TUserState => {
  switch (action.type) {
    case SET_USER:
      return setUser(state, action);
    case SET_ACCESS_TOKEN:
      return setAccessToken(state, action);
    default:
      return state;
  }
};

export default userReducer;
