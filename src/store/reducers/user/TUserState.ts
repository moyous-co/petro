import { TUser } from '../../../entities/user/TUser';

export type TUserState = {
  user: TUser | null;
  accessToken: string | null;
  isAuthorized: boolean;
};
