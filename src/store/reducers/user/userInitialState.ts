import { TUserState } from './TUserState';

const userInitialState: TUserState = {
  user: null,
  isAuthorized: false,
  accessToken: null,
};

export default userInitialState;
