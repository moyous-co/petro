import { TActionCreator } from '../../../TActionCreator';
import { TRawBoard } from '../../../../entities/board/TRawBoard';

export const SUCCESS_GET_BOARDS_LIST = 'SUCCESS_GET_BOARDS_LIST';

export const successGetBoardsList: TActionCreator<
  typeof SUCCESS_GET_BOARDS_LIST,
  TRawBoard[]
> = (payload) => ({
  type: SUCCESS_GET_BOARDS_LIST,
  payload,
});
