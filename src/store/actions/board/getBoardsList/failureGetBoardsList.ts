import { TActionCreator } from '../../../TActionCreator';

export const FAILURE_GET_BOARDS_LIST = 'FAILURE_GET_BOARDS_LIST';

export const failureGetBoardsList: TActionCreator<
  typeof FAILURE_GET_BOARDS_LIST,
  string
> = (payload) => ({
  type: FAILURE_GET_BOARDS_LIST,
  payload,
});
