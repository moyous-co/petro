import { fireGetBoardsList } from './fireGetBoardsList';
import { pendingGetBoardsList } from './pendingGetBoardsList';
import { successGetBoardsList } from './successGetBoardsList';
import { failureGetBoardsList } from './failureGetBoardsList';

export type TGetBoardsListAction = ReturnType<
  | typeof fireGetBoardsList
  | typeof pendingGetBoardsList
  | typeof successGetBoardsList
  | typeof failureGetBoardsList
>;
