import { TActionCreator } from '../../../TActionCreator';

export const PENDING_GET_BOARDS_LIST = 'PENDING_GET_BOARDS_LIST';

export const pendingGetBoardsList: TActionCreator<
  typeof PENDING_GET_BOARDS_LIST
> = () => ({
  type: PENDING_GET_BOARDS_LIST,
});
