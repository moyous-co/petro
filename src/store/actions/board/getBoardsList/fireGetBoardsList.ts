import { TActionCreator } from '../../../TActionCreator';

export const FIRE_GET_BOARDS_LIST = 'FIRE_GET_BOARDS_LIST';

export const fireGetBoardsList: TActionCreator<
  typeof FIRE_GET_BOARDS_LIST
> = () => ({
  type: FIRE_GET_BOARDS_LIST,
});
