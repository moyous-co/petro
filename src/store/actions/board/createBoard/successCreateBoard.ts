import { TActionCreator } from '../../../TActionCreator';
import { TRawBoard } from '../../../../entities/board/TRawBoard';

export const SUCCESS_CREATE_BOARD = 'SUCCESS_CREATE_BOARD';

export const successCreateBoard: TActionCreator<
  typeof SUCCESS_CREATE_BOARD,
  TRawBoard
> = (payload) => ({
  type: SUCCESS_CREATE_BOARD,
  payload,
});
