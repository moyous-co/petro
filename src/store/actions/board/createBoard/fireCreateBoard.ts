import { TActionCreator } from '../../../TActionCreator';
import { TNewBoard } from '../../../../entities/board/TNewBoard';

export const FIRE_CREATE_BOARD = 'FIRE_CREATE_BOARD';

export const fireCreateBoard: TActionCreator<
  typeof FIRE_CREATE_BOARD,
  TNewBoard
> = (payload) => ({
  type: FIRE_CREATE_BOARD,
  payload,
});
