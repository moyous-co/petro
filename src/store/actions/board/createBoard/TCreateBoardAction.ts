import { fireCreateBoard } from './fireCreateBoard';
import { pendingCreateBoard } from './pendingCreateBoard';
import { successCreateBoard } from './successCreateBoard';
import { failureCreateBoard } from './failureCreateBoard';

export type TCreateBoardAction = ReturnType<
  | typeof fireCreateBoard
  | typeof pendingCreateBoard
  | typeof successCreateBoard
  | typeof failureCreateBoard
>;
