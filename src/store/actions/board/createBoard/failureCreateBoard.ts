import { TActionCreator } from '../../../TActionCreator';

export const FAILURE_CREATE_BOARD = 'FAILURE_CREATE_BOARD';

export const failureCreateBoard: TActionCreator<
  typeof FAILURE_CREATE_BOARD,
  string
> = (payload) => ({
  type: FAILURE_CREATE_BOARD,
  payload,
});
