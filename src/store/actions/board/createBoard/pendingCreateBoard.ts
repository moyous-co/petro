import { TActionCreator } from '../../../TActionCreator';

export const PENDING_CREATE_BOARD = 'PENDING_CREATE_BOARD';

export const pendingCreateBoard: TActionCreator<
  typeof PENDING_CREATE_BOARD
> = () => ({
  type: PENDING_CREATE_BOARD,
});
