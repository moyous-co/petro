import { TActionCreator } from '../../TActionCreator';

export const FIRE_OPEN_BOARD = 'FIRE_OPEN_BOARD';

export const fireOpenBoard: TActionCreator<typeof FIRE_OPEN_BOARD, number> = (
  payload,
) => ({
  type: FIRE_OPEN_BOARD,
  payload,
});
