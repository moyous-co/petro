import { TActionCreator } from '../../../TActionCreator';

export const PENDING_DELETE_BOARD = 'PENDING_DELETE_BOARD';

export const pendingDeleteBoard: TActionCreator<
  typeof PENDING_DELETE_BOARD
> = () => ({
  type: PENDING_DELETE_BOARD,
});
