import { fireDeleteBoard } from './fireDeleteBoard';
import { pendingDeleteBoard } from './pendingDeleteBoard';
import { successDeleteBoard } from './successDeleteBoard';
import { failureDeleteBoard } from './failureDeleteBoard';

export type TDeleteBoardAction = ReturnType<
  | typeof fireDeleteBoard
  | typeof pendingDeleteBoard
  | typeof successDeleteBoard
  | typeof failureDeleteBoard
>;
