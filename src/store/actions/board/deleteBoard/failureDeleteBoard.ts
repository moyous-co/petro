import { TActionCreator } from '../../../TActionCreator';

export const FAILURE_DELETE_BOARD = 'FAILURE_DELETE_BOARD';

export const failureDeleteBoard: TActionCreator<
  typeof FAILURE_DELETE_BOARD,
  string
> = (payload) => ({
  type: FAILURE_DELETE_BOARD,
  payload,
});
