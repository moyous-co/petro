import { TActionCreator } from '../../../TActionCreator';

export const SUCCESS_DELETE_BOARD = 'SUCCESS_DELETE_BOARD';

export const successDeleteBoard: TActionCreator<
  typeof SUCCESS_DELETE_BOARD
> = () => ({
  type: SUCCESS_DELETE_BOARD,
});
