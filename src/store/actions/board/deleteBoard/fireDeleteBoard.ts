import { TActionCreator } from '../../../TActionCreator';

export const FIRE_DELETE_BOARD = 'FIRE_DELETE_BOARD';

export const fireDeleteBoard: TActionCreator<
  typeof FIRE_DELETE_BOARD,
  { id: number }
> = (payload) => ({
  type: FIRE_DELETE_BOARD,
  payload,
});
