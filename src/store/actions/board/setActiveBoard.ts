import { TActionCreator } from '../../TActionCreator';

export const SET_ACTIVE_BOARD = 'SET_ACTIVE_BOARD';

export const setActiveBoard: TActionCreator<typeof SET_ACTIVE_BOARD, number> = (
  payload,
) => ({
  type: SET_ACTIVE_BOARD,
  payload,
});
