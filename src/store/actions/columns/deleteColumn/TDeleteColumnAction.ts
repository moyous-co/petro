import { fireDeleteColumn } from './fireDeleteColumn';
import { pendingDeleteColumn } from './pendingDeleteColumn';
import { successDeleteColumn } from './successDeleteColumn';
import { failureDeleteColumn } from './failureDeleteColumn';

export type TDeleteColumnAction = ReturnType<
  | typeof fireDeleteColumn
  | typeof pendingDeleteColumn
  | typeof successDeleteColumn
  | typeof failureDeleteColumn
>;
