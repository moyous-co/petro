import { TActionCreator } from '../../../TActionCreator';

export const FIRE_DELETE_COLUMN = 'FIRE_DELETE_COLUMN';

export const fireDeleteColumn: TActionCreator<
  typeof FIRE_DELETE_COLUMN,
  { id: number }
> = (payload) => ({
  type: FIRE_DELETE_COLUMN,
  payload,
});
