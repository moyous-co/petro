import { TActionCreator } from '../../../TActionCreator';

export const FAILURE_DELETE_COLUMN = 'FAILURE_DELETE_COLUMN';

export const failureDeleteColumn: TActionCreator<
  typeof FAILURE_DELETE_COLUMN,
  string
> = (payload) => ({
  type: FAILURE_DELETE_COLUMN,
  payload,
});
