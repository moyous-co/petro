import { TActionCreator } from '../../../TActionCreator';
import { TCommonDeleteResponse } from '../../../../api/TCommonDeleteResponse';

export const SUCCESS_DELETE_COLUMN = 'SUCCESS_DELETE_COLUMN';

export const successDeleteColumn: TActionCreator<
  typeof SUCCESS_DELETE_COLUMN,
  TCommonDeleteResponse
> = (payload) => ({
  type: SUCCESS_DELETE_COLUMN,
  payload,
});
