import { TActionCreator } from '../../../TActionCreator';

export const PENDING_DELETE_COLUMN = 'PENDING_DELETE_COLUMN';

export const pendingDeleteColumn: TActionCreator<
  typeof PENDING_DELETE_COLUMN
> = () => ({
  type: PENDING_DELETE_COLUMN,
});
