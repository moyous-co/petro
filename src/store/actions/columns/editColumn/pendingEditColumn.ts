import { TActionCreator } from '../../../TActionCreator';

export const PENDING_EDIT_COLUMN = 'PENDING_EDIT_COLUMN';

export const pendingEditColumn: TActionCreator<
  typeof PENDING_EDIT_COLUMN
> = () => ({
  type: PENDING_EDIT_COLUMN,
});
