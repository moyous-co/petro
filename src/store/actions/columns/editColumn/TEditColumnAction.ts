import { fireEditColumn } from './fireEditColumn';
import { pendingEditColumn } from './pendingEditColumn';
import { successEditColumn } from './successEditColumn';
import { failureEditColumn } from './failureEditColumn';

export type TEditColumnAction = ReturnType<
  | typeof fireEditColumn
  | typeof pendingEditColumn
  | typeof successEditColumn
  | typeof failureEditColumn
>;
