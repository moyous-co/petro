import { TActionCreator } from '../../../TActionCreator';
import { TEditColumn } from '../../../../entities/column/TEditColumn';

export const FIRE_EDIT_COLUMN = 'FIRE_EDIT_COLUMN';

export const fireEditColumn: TActionCreator<
  typeof FIRE_EDIT_COLUMN,
  TEditColumn
> = (payload) => ({
  type: FIRE_EDIT_COLUMN,
  payload,
});
