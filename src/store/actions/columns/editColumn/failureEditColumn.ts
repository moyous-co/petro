import { TActionCreator } from '../../../TActionCreator';

export const FAILURE_EDIT_COLUMN = 'FAILURE_EDIT_COLUMN';

export const failureEditColumn: TActionCreator<
  typeof FAILURE_EDIT_COLUMN,
  string
> = (payload) => ({
  type: FAILURE_EDIT_COLUMN,
  payload,
});
