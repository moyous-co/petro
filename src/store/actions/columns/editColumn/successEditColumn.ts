import { TActionCreator } from '../../../TActionCreator';
import { TRawColumn } from '../../../../entities/column/TRawColumn';

export const SUCCESS_EDIT_COLUMN = 'SUCCESS_EDIT_COLUMN';

export const successEditColumn: TActionCreator<
  typeof SUCCESS_EDIT_COLUMN,
  TRawColumn
> = (payload) => ({
  type: SUCCESS_EDIT_COLUMN,
  payload,
});
