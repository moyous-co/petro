import { fireGetColumns } from './fireGetColumns';
import { pendingGetColumns } from './pendingGetColumns';
import { successGetColumns } from './successGetColumns';
import { failureGetColumns } from './failureGetColumns';

export type TGetColumnsAction = ReturnType<
  | typeof fireGetColumns
  | typeof pendingGetColumns
  | typeof successGetColumns
  | typeof failureGetColumns
>;
