import { TActionCreator } from '../../../TActionCreator';

export const FIRE_GET_COLUMNS = 'FIRE_GET_COLUMNS';

export const fireGetColumns: TActionCreator<typeof FIRE_GET_COLUMNS, number> = (
  payload,
) => ({
  type: FIRE_GET_COLUMNS,
  payload,
});
