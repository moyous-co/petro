import { TActionCreator } from '../../../TActionCreator';

export const PENDING_GET_COLUMNS = 'PENDING_GET_COLUMNS';

export const pendingGetColumns: TActionCreator<
  typeof PENDING_GET_COLUMNS
> = () => ({
  type: PENDING_GET_COLUMNS,
});
