import { TActionCreator } from '../../../TActionCreator';

export const FAILURE_GET_COLUMNS = 'FAILURE_GET_COLUMNS';

export const failureGetColumns: TActionCreator<
  typeof FAILURE_GET_COLUMNS,
  string
> = (payload) => ({
  type: FAILURE_GET_COLUMNS,
  payload,
});
