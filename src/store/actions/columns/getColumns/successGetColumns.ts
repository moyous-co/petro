import { TActionCreator } from '../../../TActionCreator';
import { TRawColumn } from '../../../../entities/column/TRawColumn';

export const SUCCESS_GET_COLUMNS = 'SUCCESS_GET_COLUMNS';

export const successGetColumns: TActionCreator<
  typeof SUCCESS_GET_COLUMNS,
  TRawColumn[]
> = (payload) => ({
  type: SUCCESS_GET_COLUMNS,
  payload,
});
