import { TActionCreator } from '../../../TActionCreator';

export const PENDING_CREATE_COLUMN = 'PENDING_CREATE_COLUMN';

export const pendingCreateColumn: TActionCreator<
  typeof PENDING_CREATE_COLUMN
> = () => ({
  type: PENDING_CREATE_COLUMN,
});
