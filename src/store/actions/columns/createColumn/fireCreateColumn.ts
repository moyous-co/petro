import { TActionCreator } from '../../../TActionCreator';
import { TNewColumn } from '../../../../entities/column/TNewColumn';

export const FIRE_CREATE_COLUMN = 'FIRE_CREATE_COLUMN';

export const fireCreateColumn: TActionCreator<
  typeof FIRE_CREATE_COLUMN,
  TNewColumn
> = (payload) => ({
  type: FIRE_CREATE_COLUMN,
  payload,
});
