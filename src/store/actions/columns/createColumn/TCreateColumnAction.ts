import { fireCreateColumn } from './fireCreateColumn';
import { pendingCreateColumn } from './pendingCreateColumn';
import { successCreateColumn } from './successCreateColumn';
import { failureCreateColumn } from './failureCreateColumn';

export type TCreateColumnAction = ReturnType<
  | typeof fireCreateColumn
  | typeof pendingCreateColumn
  | typeof successCreateColumn
  | typeof failureCreateColumn
>;
