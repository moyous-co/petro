import { TActionCreator } from '../../../TActionCreator';

export const FAILURE_CREATE_COLUMN = 'FAILURE_CREATE_COLUMN';

export const failureCreateColumn: TActionCreator<
  typeof FAILURE_CREATE_COLUMN,
  string
> = (payload) => ({
  type: FAILURE_CREATE_COLUMN,
  payload,
});
