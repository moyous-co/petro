import { TActionCreator } from '../../../TActionCreator';
import { TRawColumn } from '../../../../entities/column/TRawColumn';

export const SUCCESS_CREATE_COLUMN = 'SUCCESS_CREATE_COLUMN';

export const successCreateColumn: TActionCreator<
  typeof SUCCESS_CREATE_COLUMN,
  TRawColumn
> = (payload) => ({
  type: SUCCESS_CREATE_COLUMN,
  payload,
});
