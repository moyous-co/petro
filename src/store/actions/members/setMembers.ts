import { TActionCreator } from '../../TActionCreator';
import { TMember } from '../../../entities/member/TMember';

export const SET_MEMBERS = 'SET_MEMBERS';

export const setMembers: TActionCreator<typeof SET_MEMBERS, TMember[]> = (
  payload,
) => ({
  type: SET_MEMBERS,
  payload,
});
