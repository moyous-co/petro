import { TActionCreator } from '../../TActionCreator';

export const SET_ACCESS_TOKEN = 'SET_ACCESS_TOKEN';

export const setAccessToken: TActionCreator<typeof SET_ACCESS_TOKEN, string> = (
  payload,
) => ({
  type: SET_ACCESS_TOKEN,
  payload,
});
