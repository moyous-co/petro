import { TActionCreator } from '../../TActionCreator';
import { EPanelId } from '../../../lib/consts';

export const SET_ACTIVE_PANEL = 'SET_ACTIVE_PANEL';

export const setActivePanel: TActionCreator<
  typeof SET_ACTIVE_PANEL,
  EPanelId
> = (payload) => ({
  type: SET_ACTIVE_PANEL,
  payload,
});
