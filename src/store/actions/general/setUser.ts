import { TActionCreator } from '../../TActionCreator';
import { TUser } from '../../../entities/user/TUser';

export const SET_USER = 'SET_USER';

export const setUser: TActionCreator<typeof SET_USER, TUser> = (payload) => ({
  type: SET_USER,
  payload,
});
