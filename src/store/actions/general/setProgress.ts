import { TActionCreator } from '../../TActionCreator';

export const SET_PROGRESS = 'SET_PROGRESS';

export const setProgress: TActionCreator<
  typeof SET_PROGRESS,
  {
    progress: number;
    title: string;
  }
> = (payload) => ({
  type: SET_PROGRESS,
  payload,
});
