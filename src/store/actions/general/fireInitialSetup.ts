import { TActionCreator } from '../../TActionCreator';

export const FIRE_INITIAL_SETUP = 'FIRE_INITIAL_SETUP';

export const fireInitialSetup: TActionCreator<
  typeof FIRE_INITIAL_SETUP
> = () => ({
  type: FIRE_INITIAL_SETUP,
});
