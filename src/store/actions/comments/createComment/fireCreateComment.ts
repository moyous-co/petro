import { TActionCreator } from '../../../TActionCreator';
import { TNewComment } from '../../../../entities/comment/TNewComment';

export const FIRE_CREATE_COMMENT = 'FIRE_CREATE_COMMENT';

export const fireCreateComment: TActionCreator<
  typeof FIRE_CREATE_COMMENT,
  TNewComment
> = (payload) => ({
  type: FIRE_CREATE_COMMENT,
  payload,
});
