import { TActionCreator } from '../../../TActionCreator';
import { TRawComment } from '../../../../entities/comment/TRawComment';

export const SUCCESS_CREATE_COMMENT = 'SUCCESS_CREATE_COMMENT';

export const successCreateComment: TActionCreator<
  typeof SUCCESS_CREATE_COMMENT,
  TRawComment
> = (payload) => ({
  type: SUCCESS_CREATE_COMMENT,
  payload,
});
