import { TActionCreator } from '../../../TActionCreator';

export const PENDING_CREATE_COMMENT = 'PENDING_CREATE_COMMENT';

export const pendingCreateComment: TActionCreator<
  typeof PENDING_CREATE_COMMENT
> = () => ({
  type: PENDING_CREATE_COMMENT,
});
