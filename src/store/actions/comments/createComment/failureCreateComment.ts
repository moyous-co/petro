import { TActionCreator } from '../../../TActionCreator';

export const FAILURE_CREATE_COMMENT = 'FAILURE_CREATE_COMMENT';

export const failureCreateComment: TActionCreator<
  typeof FAILURE_CREATE_COMMENT,
  string
> = (payload) => ({
  type: FAILURE_CREATE_COMMENT,
  payload,
});
