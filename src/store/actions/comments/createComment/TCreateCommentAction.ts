import { fireCreateComment } from './fireCreateComment';
import { pendingCreateComment } from './pendingCreateComment';
import { successCreateComment } from './successCreateComment';
import { failureCreateComment } from './failureCreateComment';

export type TCreateCommentAction = ReturnType<
  | typeof fireCreateComment
  | typeof pendingCreateComment
  | typeof successCreateComment
  | typeof failureCreateComment
>;
