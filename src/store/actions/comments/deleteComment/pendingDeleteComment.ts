import { TActionCreator } from '../../../TActionCreator';

export const PENDING_DELETE_COMMENT = 'PENDING_DELETE_COMMENT';

export const pendingDeleteComment: TActionCreator<
  typeof PENDING_DELETE_COMMENT
> = () => ({
  type: PENDING_DELETE_COMMENT,
});
