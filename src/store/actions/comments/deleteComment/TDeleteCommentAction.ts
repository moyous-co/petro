import { fireDeleteComment } from './fireDeleteComment';
import { pendingDeleteComment } from './pendingDeleteComment';
import { successDeleteComment } from './successDeleteComment';
import { failureDeleteComment } from './failureDeleteComment';

export type TDeleteCommentAction = ReturnType<
  | typeof fireDeleteComment
  | typeof pendingDeleteComment
  | typeof successDeleteComment
  | typeof failureDeleteComment
>;
