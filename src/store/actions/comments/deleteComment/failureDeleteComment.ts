import { TActionCreator } from '../../../TActionCreator';

export const FAILURE_DELETE_COMMENT = 'FAILURE_DELETE_COMMENT';

export const failureDeleteComment: TActionCreator<
  typeof FAILURE_DELETE_COMMENT,
  string
> = (payload) => ({
  type: FAILURE_DELETE_COMMENT,
  payload,
});
