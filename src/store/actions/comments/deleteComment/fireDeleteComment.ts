import { TActionCreator } from '../../../TActionCreator';

export const FIRE_DELETE_COMMENT = 'FIRE_DELETE_COMMENT';

export const fireDeleteComment: TActionCreator<
  typeof FIRE_DELETE_COMMENT,
  { id: number; cardId: number }
> = (payload) => ({
  type: FIRE_DELETE_COMMENT,
  payload,
});
