import { TActionCreator } from '../../../TActionCreator';
import { TCommonDeleteResponse } from '../../../../api/TCommonDeleteResponse';

export const SUCCESS_DELETE_COMMENT = 'SUCCESS_DELETE_COMMENT';

export const successDeleteComment: TActionCreator<
  typeof SUCCESS_DELETE_COMMENT,
  TCommonDeleteResponse & { cardId: number }
> = (payload) => ({
  type: SUCCESS_DELETE_COMMENT,
  payload,
});
