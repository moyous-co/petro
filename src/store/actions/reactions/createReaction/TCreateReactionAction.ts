import { fireCreateReaction } from './fireCreateReaction';
import { pendingCreateReaction } from './pendingCreateReaction';
import { successCreateReaction } from './successCreateReaction';
import { failureCreateReaction } from './failureCreateReaction';

export type TCreateReactionAction = ReturnType<
  | typeof fireCreateReaction
  | typeof pendingCreateReaction
  | typeof successCreateReaction
  | typeof failureCreateReaction
>;
