import { TActionCreator } from '../../../TActionCreator';

export const FAILURE_CREATE_REACTION = 'FAILURE_CREATE_REACTION';

export const failureCreateReaction: TActionCreator<
  typeof FAILURE_CREATE_REACTION,
  string
> = (payload) => ({
  type: FAILURE_CREATE_REACTION,
  payload,
});
