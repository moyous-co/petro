import { TActionCreator } from '../../../TActionCreator';
import { TRawReaction } from '../../../../entities/reaction/TRawReaction';

export const SUCCESS_CREATE_REACTION = 'SUCCESS_CREATE_REACTION';

export const successCreateReaction: TActionCreator<
  typeof SUCCESS_CREATE_REACTION,
  TRawReaction
> = (payload) => ({
  type: SUCCESS_CREATE_REACTION,
  payload,
});
