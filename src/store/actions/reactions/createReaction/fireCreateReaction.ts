import { TActionCreator } from '../../../TActionCreator';
import { TNewReaction } from '../../../../entities/reaction/TNewReaction';

export const FIRE_CREATE_REACTION = 'FIRE_CREATE_REACTION';

export const fireCreateReaction: TActionCreator<
  typeof FIRE_CREATE_REACTION,
  TNewReaction
> = (payload) => ({
  type: FIRE_CREATE_REACTION,
  payload,
});
