import { TActionCreator } from '../../../TActionCreator';

export const PENDING_CREATE_REACTION = 'PENDING_CREATE_REACTION';

export const pendingCreateReaction: TActionCreator<
  typeof PENDING_CREATE_REACTION
> = () => ({
  type: PENDING_CREATE_REACTION,
});
