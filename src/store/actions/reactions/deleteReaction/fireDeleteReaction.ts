import { TActionCreator } from '../../../TActionCreator';

export const FIRE_DELETE_REACTION = 'FIRE_DELETE_REACTION';

export const fireDeleteReaction: TActionCreator<
  typeof FIRE_DELETE_REACTION,
  { id: number; cardId: number }
> = (payload) => ({
  type: FIRE_DELETE_REACTION,
  payload,
});
