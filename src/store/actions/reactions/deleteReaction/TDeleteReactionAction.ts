import { fireDeleteReaction } from './fireDeleteReaction';
import { pendingDeleteReaction } from './pendingDeleteReaction';
import { successDeleteReaction } from './successDeleteReaction';
import { failureDeleteReaction } from './failureDeleteReaction';

export type TDeleteReactionAction = ReturnType<
  | typeof fireDeleteReaction
  | typeof pendingDeleteReaction
  | typeof successDeleteReaction
  | typeof failureDeleteReaction
>;
