import { TActionCreator } from '../../../TActionCreator';

export const PENDING_DELETE_REACTION = 'PENDING_DELETE_REACTION';

export const pendingDeleteReaction: TActionCreator<
  typeof PENDING_DELETE_REACTION
> = () => ({
  type: PENDING_DELETE_REACTION,
});
