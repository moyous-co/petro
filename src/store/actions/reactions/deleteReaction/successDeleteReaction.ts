import { TActionCreator } from '../../../TActionCreator';
import { TCommonDeleteResponse } from '../../../../api/TCommonDeleteResponse';

export const SUCCESS_DELETE_REACTION = 'SUCCESS_DELETE_REACTION';

export const successDeleteReaction: TActionCreator<
  typeof SUCCESS_DELETE_REACTION,
  TCommonDeleteResponse & { cardId: number }
> = (payload) => ({
  type: SUCCESS_DELETE_REACTION,
  payload,
});
