import { TActionCreator } from '../../../TActionCreator';

export const FAILURE_DELETE_REACTION = 'FAILURE_DELETE_REACTION';

export const failureDeleteReaction: TActionCreator<
  typeof FAILURE_DELETE_REACTION,
  string
> = (payload) => ({
  type: FAILURE_DELETE_REACTION,
  payload,
});
