import { TActionCreator } from '../../../TActionCreator';

export const FIRE_DELETE_CARD = 'FIRE_DELETE_CARD';

export const fireDeleteCard: TActionCreator<
  typeof FIRE_DELETE_CARD,
  { id: number; column_id: number }
> = (payload) => ({
  type: FIRE_DELETE_CARD,
  payload,
});
