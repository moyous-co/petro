import { TActionCreator } from '../../../TActionCreator';
import { TCommonDeleteResponse } from '../../../../api/TCommonDeleteResponse';

export const SUCCESS_DELETE_CARD = 'SUCCESS_DELETE_CARD';

export const successDeleteCard: TActionCreator<
  typeof SUCCESS_DELETE_CARD,
  TCommonDeleteResponse & { columnId: number }
> = (payload) => ({
  type: SUCCESS_DELETE_CARD,
  payload,
});
