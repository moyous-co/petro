import { fireDeleteCard } from './fireDeleteCard';
import { pendingDeleteCard } from './pendingDeleteCard';
import { successDeleteCard } from './successDeleteCard';
import { failureDeleteCard } from './failureDeleteCard';

export type TDeleteCardAction = ReturnType<
  | typeof fireDeleteCard
  | typeof pendingDeleteCard
  | typeof successDeleteCard
  | typeof failureDeleteCard
>;
