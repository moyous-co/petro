import { TActionCreator } from '../../../TActionCreator';

export const PENDING_DELETE_CARD = 'PENDING_DELETE_CARD';

export const pendingDeleteCard: TActionCreator<
  typeof PENDING_DELETE_CARD
> = () => ({
  type: PENDING_DELETE_CARD,
});
