import { TActionCreator } from '../../../TActionCreator';

export const FAILURE_DELETE_CARD = 'FAILURE_DELETE_CARD';

export const failureDeleteCard: TActionCreator<
  typeof FAILURE_DELETE_CARD,
  string
> = (payload) => ({
  type: FAILURE_DELETE_CARD,
  payload,
});
