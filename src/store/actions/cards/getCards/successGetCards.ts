import { TActionCreator } from '../../../TActionCreator';
import { TRawCard } from '../../../../entities/card/TRawCard';

export const SUCCESS_GET_CARDS = 'SUCCESS_GET_CARDS';

export const successGetCards: TActionCreator<
  typeof SUCCESS_GET_CARDS,
  TRawCard[]
> = (payload) => ({
  type: SUCCESS_GET_CARDS,
  payload,
});
