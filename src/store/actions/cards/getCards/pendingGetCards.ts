import { TActionCreator } from '../../../TActionCreator';

export const PENDING_GET_CARDS = 'PENDING_GET_CARDS';

export const pendingGetCards: TActionCreator<
  typeof PENDING_GET_CARDS
> = () => ({
  type: PENDING_GET_CARDS,
});
