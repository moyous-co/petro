import { fireGetCards } from './fireGetCards';
import { pendingGetCards } from './pendingGetCards';
import { successGetCards } from './successGetCards';
import { failureGetCards } from './failureGetCards';

export type TGetCardsAction = ReturnType<
  | typeof fireGetCards
  | typeof pendingGetCards
  | typeof successGetCards
  | typeof failureGetCards
>;
