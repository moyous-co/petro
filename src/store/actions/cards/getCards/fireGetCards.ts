import { TActionCreator } from '../../../TActionCreator';

export const FIRE_GET_CARDS = 'FIRE_GET_CARDS';

export const fireGetCards: TActionCreator<typeof FIRE_GET_CARDS, number> = (
  payload,
) => ({
  type: FIRE_GET_CARDS,
  payload,
});
