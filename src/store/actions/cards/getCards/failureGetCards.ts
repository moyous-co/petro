import { TActionCreator } from '../../../TActionCreator';

export const FAILURE_GET_CARDS = 'FAILURE_GET_CARDS';

export const failureGetCards: TActionCreator<
  typeof FAILURE_GET_CARDS,
  string
> = (payload) => ({
  type: FAILURE_GET_CARDS,
  payload,
});
