import { TActionCreator } from '../../../TActionCreator';
import { TRawCard } from '../../../../entities/card/TRawCard';

export const SUCCESS_EDIT_CARD = 'SUCCESS_EDIT_CARD';

export const successEditCard: TActionCreator<
  typeof SUCCESS_EDIT_CARD,
  TRawCard
> = (payload) => ({
  type: SUCCESS_EDIT_CARD,
  payload,
});
