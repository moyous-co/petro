import { TActionCreator } from '../../../TActionCreator';

export const FAILURE_EDIT_CARD = 'FAILURE_EDIT_CARD';

export const failureEditCard: TActionCreator<
  typeof FAILURE_EDIT_CARD,
  string
> = (payload) => ({
  type: FAILURE_EDIT_CARD,
  payload,
});
