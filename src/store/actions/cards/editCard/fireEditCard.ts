import { TActionCreator } from '../../../TActionCreator';
import { TEditCard } from '../../../../entities/card/TEditCard';

export const FIRE_EDIT_CARD = 'FIRE_EDIT_CARD';

export const fireEditCard: TActionCreator<typeof FIRE_EDIT_CARD, TEditCard> = (
  payload,
) => ({
  type: FIRE_EDIT_CARD,
  payload,
});
