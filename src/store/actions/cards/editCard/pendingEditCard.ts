import { TActionCreator } from '../../../TActionCreator';

export const PENDING_EDIT_CARD = 'PENDING_EDIT_CARD';

export const pendingEditCard: TActionCreator<
  typeof PENDING_EDIT_CARD
> = () => ({
  type: PENDING_EDIT_CARD,
});
