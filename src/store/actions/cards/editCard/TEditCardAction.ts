import { fireEditCard } from './fireEditCard';
import { pendingEditCard } from './pendingEditCard';
import { successEditCard } from './successEditCard';
import { failureEditCard } from './failureEditCard';

export type TEditCardAction = ReturnType<
  | typeof fireEditCard
  | typeof pendingEditCard
  | typeof successEditCard
  | typeof failureEditCard
>;
