import { fireCreateCard } from './fireCreateCard';
import { pendingCreateCard } from './pendingCreateCard';
import { successCreateCard } from './successCreateCard';
import { failureCreateCard } from './failureCreateCard';

export type TCreateCardAction = ReturnType<
  | typeof fireCreateCard
  | typeof pendingCreateCard
  | typeof successCreateCard
  | typeof failureCreateCard
>;
