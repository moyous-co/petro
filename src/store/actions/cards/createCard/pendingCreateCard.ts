import { TActionCreator } from '../../../TActionCreator';

export const PENDING_CREATE_CARD = 'PENDING_CREATE_CARD';

export const pendingCreateCard: TActionCreator<
  typeof PENDING_CREATE_CARD
> = () => ({
  type: PENDING_CREATE_CARD,
});
