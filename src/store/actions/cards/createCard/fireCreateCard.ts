import { TActionCreator } from '../../../TActionCreator';
import { TNewCard } from '../../../../entities/card/TNewCard';

export const FIRE_CREATE_CARD = 'FIRE_CREATE_CARD';

export const fireCreateCard: TActionCreator<
  typeof FIRE_CREATE_CARD,
  TNewCard
> = (payload) => ({
  type: FIRE_CREATE_CARD,
  payload,
});
