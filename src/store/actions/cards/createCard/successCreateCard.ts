import { TActionCreator } from '../../../TActionCreator';
import { TRawCard } from '../../../../entities/card/TRawCard';

export const SUCCESS_CREATE_CARD = 'SUCCESS_CREATE_CARD';

export const successCreateCard: TActionCreator<
  typeof SUCCESS_CREATE_CARD,
  TRawCard
> = (payload) => ({
  type: SUCCESS_CREATE_CARD,
  payload,
});
