import { TActionCreator } from '../../../TActionCreator';

export const FAILURE_CREATE_CARD = 'FAILURE_CREATE_CARD';

export const failureCreateCard: TActionCreator<
  typeof FAILURE_CREATE_CARD,
  string
> = (payload) => ({
  type: FAILURE_CREATE_CARD,
  payload,
});
