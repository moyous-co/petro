import { TAppState } from './TAppState';
import userInitialState from './reducers/user/userInitialState';
import generalInitialState from './reducers/general/generalInitialState';
import boardsListInitialState from './reducers/boardsList/boardsListInitialState';
import activeBoardInitialState from './reducers/activeBoard/activeBoardInitialState';
import cardsInitialState from './reducers/cards/cardsInitialState';
import columnsInitialState from './reducers/columns/columnsInitialState';
import reactionsInitialState from './reducers/reactions/reactionsInitialState';
import commentsInitialState from './reducers/comments/commentsInitialState';
import membersInitialState from './reducers/members/membersInitialState';

const appInitialState: TAppState = {
  user: userInitialState,
  general: generalInitialState,
  boardsList: boardsListInitialState,
  activeBoard: activeBoardInitialState,
  cards: cardsInitialState,
  columns: columnsInitialState,
  reactions: reactionsInitialState,
  comments: commentsInitialState,
  members: membersInitialState,
};

export default appInitialState;
