import { TUserState } from './reducers/user/TUserState';
import { TGeneralState } from './reducers/general/TGeneralState';
import { TBoardsListState } from './reducers/boardsList/TBoardsListState';
import { TActiveBoardState } from './reducers/activeBoard/TActiveBoardState';
import { TCardsState } from './reducers/cards/TCardsState';
import { TColumnsState } from './reducers/columns/TColumnsState';
import { TReactionsState } from './reducers/reactions/TReactionsState';
import { TCommentsState } from './reducers/comments/TCommentsState';
import { TMembersState } from './reducers/members/TMembersState';

export type TAppState = {
  user: TUserState;
  general: TGeneralState;
  boardsList: TBoardsListState;
  activeBoard: TActiveBoardState;
  cards: TCardsState;
  columns: TColumnsState;
  reactions: TReactionsState;
  comments: TCommentsState;
  members: TMembersState;
};
