import React, { FC, ReactElement, useEffect, useState } from 'react';
import View from '@vkontakte/vkui/dist/components/View/View';
import '@vkontakte/vkui/dist/vkui.css';
import { useDispatch, useSelector } from 'react-redux';
import { AppRoot } from '@vkontakte/vkui';
import bridge from '@vkontakte/vk-bridge';
import { EPanelId } from './lib/consts';
import ActiveBoardPanel from './panels/ActiveBoard/ActiveBoardPanel';
import InitialPanel from './panels/Initial/InitialPanel';
import selectActivePanel from './store/selectors/selectActivePanel';
import { fireInitialSetup } from './store/actions/general/fireInitialSetup';
import DashboardPanel from './panels/Dashboard/DashboardPanel';
import CreateBoardPanel from './panels/CreateBoard/CreateBoardPanel';
import { setMembers } from './store/actions/members/setMembers';
import cleanMember from './entities/member/cleanMember';
import { TRawUser } from './entities/user/TRawUser';

const App: FC = () => {
  const dispatch = useDispatch();
  const activePanel = useSelector(selectActivePanel);
  const [popout] = useState<ReactElement | null>(null);

  useEffect(() => {
    bridge.subscribe(({ detail: { type, data } }) => {
      if (type === 'VKWebAppCallAPIMethodResult') {
        if ((data as any).response) {
          const members: TRawUser[] = (data as any).response || [];

          dispatch(setMembers(members.map((member) => cleanMember(member))));
        }
      }
    });
  }, [dispatch]);

  useEffect(() => {
    dispatch(fireInitialSetup());
  }, [dispatch]);

  return (
    <AppRoot>
      <View activePanel={activePanel} popout={popout}>
        <InitialPanel id={EPanelId.INITIAL} />
        <DashboardPanel id={EPanelId.DASHBOARD} />
        <ActiveBoardPanel id={EPanelId.ACTIVE_BOARD} />
        <CreateBoardPanel id={EPanelId.CREATE_BOARD} />
      </View>
    </AppRoot>
  );
};

export default App;
