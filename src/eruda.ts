// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import * as eruda from 'eruda';
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import * as erudaCode from 'eruda-code';
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import * as erudaDom from 'eruda-dom';

eruda.init();
eruda.add(erudaCode);
eruda.add(erudaDom);

export default eruda;
