import React, { FC, memo } from 'react';
import { Provider } from 'react-redux';
import useStore from './store/useStore';
import App from './App';

const Root: FC = () => {
  const store = useStore();

  return (
    <Provider store={store}>
      <App />
    </Provider>
  );
};

export default memo(Root);
