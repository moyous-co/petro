export type TCommonResponse<TPayload> =
  | {
      hasError: false;
      payload: TPayload;
    }
  | {
      hasError: true;
      error: string;
    };
