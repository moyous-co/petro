import { API_URL } from '../lib/consts';
import { TCommonResponse } from './TCommonResponse';
import { TCommonParameters } from './TCommonParameters';

const commonFetch = async <
  U extends Record<string, unknown>,
  T extends Record<string, string> = Record<string, string>
>(
  act: string,
  parameters: T & TCommonParameters,
): Promise<TCommonResponse<U>> => {
  try {
    const url = new URL(`${API_URL}`);
    url.search = new URLSearchParams({
      act,
      ...parameters,
    }).toString();

    const response = await fetch(url.toString());

    const responseJson = await response.json();

    return {
      ...responseJson,
      hasError: Object.prototype.hasOwnProperty.call(responseJson, 'error'),
    };
  } catch (e) {
    return {
      hasError: true,
      error: 'Неопознанная ошибка',
    };
  }
};

export default commonFetch;
